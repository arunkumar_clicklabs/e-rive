var utils = require('./../utils/commonfunction');
var generatePassword = require('password-generator');
var moment = require('moment');
var logging = require('./logging');
var constants = require('./constants');
var mailer = require('./mailer');
var messenger = require('./messenger');
var user = require('./user');
var responses = require('./responses');
var make_payments = require('./make_payments');
var async = require('async');
var use_otp = true;

exports.login_fb = function(req, res) {
    logging.startSection("login_fb");
    logging.logRequest(req);

    var fbId = req.body.user_fb_id;
    var fbName = req.body.user_fb_name;
    var fbAccessToken = req.body.fb_access_token;
    var latitude = req.body.latitude;
    var longitude = req.body.longitude;
    var deviceToken = req.body.device_token;
    var deviceType = req.body.device_type;
    var appVersion = req.body.app_version;
    var country = req.body.country;
    var osVersion = req.body.os_version;
    var deviceName = req.body.device_name;
    var fbmail = req.body.fb_mail;
    var phoneNo = req.body.ph_no;
    var password = req.body.password;
    var username = req.body.username;
    var oneTimePassword = req.body.otp;
    var code_used = req.body.referral_code;
    var unique_device_id = req.body.unique_device_id;
    var nounce = req.body.nounce;

    code_used = code_used ? code_used.toUpperCase() : "";
    if (typeof unique_device_id === 'undefined') {
        unique_device_id = 'Not sent, app version: ' + appVersion;
    } else if (unique_device_id === 'not_found') {
        unique_device_id += ', app version: ' + appVersion;
    }

    var manValues = [fbId, deviceType, fbName];
    var checkData = utils.checkBlank(manValues);

    if (checkData == 1) {
        var response = {
            "error": 'Some parameter missing',
            "flag": 0
        };
        logging.logResponse(response);
        res.send(JSON.stringify(response));
    } else {
        if (fbmail == "") {
            if (username != "") {
                fbmail = username + '@facebook.com';
            } else {
                fbmail = fbId + '@facebook.com';
            }
        }

        var email_ids = [];
        email_ids.push(fbmail);

        checkFbUser(fbId, fbmail, fbAccessToken, function(results) {
            if (results == 1) {
                var response = {
                    "error": "Facebook error. Please sign in again",
                    "flag": 1
                };
                logging.logResponse(response);
                res.send(JSON.stringify(response));
            } else {
                if (results.length == 0) {
                    //register

                    var sql = "SELECT user_id,phone_no,access_token,user_image,user_email,user_name,referral_code,verification_token,verification_status,current_user_status,user_device_token,reg_as,can_change_location,can_schedule,is_available " +
                        "FROM tb_users WHERE user_email=? LIMIT 1";
                    connection.query(sql, [fbmail], function(err, responseMail) {
                        if (responseMail.length == 1) {
                            if ((responseMail[0].phone_no != '') && (responseMail[0].verification_status == 1)) {
                                var current_user_status1;
                                var date2 = new Date();
                                if (responseMail[0].reg_as == 0) { // If register as customer....then login as customer
                                    current_user_status1 = 2;
                                    var sql01 = "UPDATE `tb_users` set `user_fb_id`=? ,`current_user_status`=? where `user_id`=? limit 1";
                                    connection.query(sql01, [fbId, current_user_status1, responseMail[0].user_id], function(err, result) {});
                                }
                                var userId = responseMail[0].user_id;
                                updateUserParams(deviceToken, latitude, longitude, country, osVersion, deviceName, deviceType, userId);
                                checkAppVersion(userId, appVersion, deviceType, function(updateAppPopUp) {
                                    var sql03 = "SELECT * FROM `tb_fare`";
                                    connection.query(sql03, function(err, fare_details) {
                                        var response1 = {
                                            "user_data": {
                                                "access_token": responseMail[0].access_token,
                                                "user_name": responseMail[0].user_name,
                                                "user_email": responseMail[0].user_email,
                                                "user_image": responseMail[0].user_image,
                                                "phone_no": responseMail[0].phone_no,
                                                "referral_code": responseMail[0].referral_code,
                                                "can_change_location": responseMail[0].can_change_location,
                                                "can_schedule": responseMail[0].can_schedule,
                                                "scheduling_limit": constants.SCHEDULING_LIMIT,
                                                "is_available": responseMail[0].is_available,
                                                "gcm_intent": constants.GCM_INTENT_FLAG,
                                                "nukkad_enable": constants.NUKKAD_ENABLE_FLAG,
                                                "nukkad_icon": constants.NUKKAD_BUTTON_ICON,
                                                "current_user_status": current_user_status1,
                                                "fare_details": fare_details,
                                                "exceptional_driver": responseMail[0].reg_as
                                            },
                                            "popup": updateAppPopUp
                                        };
                                        logging.logResponse(response1);
                                        res.send(JSON.stringify(response1));
                                    });
                                });


                            } else {
                                if ((oneTimePassword == "") && (responseMail[0].verification_status == 0) && (responseMail[0].verification_token != "")) {
                                    checkAppVersion(responseMail[0].user_id, appVersion, deviceType, function(updateAppPopUp) {
                                        var response1 = {
                                            "error": 'Please enter OTP',
                                            "flag": 2,
                                            "popup": updateAppPopUp,
                                            "phone_no": phoneNo
                                        };
                                        logging.logResponse(response1);
                                        res.send(JSON.stringify(response1));
                                    });
                                }
                                if ((responseMail[0].user_mobile_no == "") && (phoneNo == "") && (responseMail[0].verification_token == "")) {
                                    checkAppVersion(responseMail[0].user_id, appVersion, deviceType, function(updateAppPopUp) {
                                        var response = {
                                            "error": 'Please enter details',
                                            "flag": 3,
                                            "popup": updateAppPopUp
                                        };
                                        logging.logResponse(response);
                                        res.send(JSON.stringify(response));
                                    });
                                } else if ((responseMail[0].phone_no == "") && (phoneNo != "")) {
                                    //var otp = "4444";
                                    var otp = utils.generateRandomString();
                                    messenger.sendOTP(responseMail[0].user_name, phoneNo, otp);

                                    var md5 = require('MD5');
                                    var hash = md5(password);
                                    checkAppVersion(responseMail[0].user_id, appVersion, deviceType, function(updateAppPopUp) {
                                        var sql = "UPDATE tb_users set user_fb_id=?,user_name=?,phone_no=?,user_email=?,password = ?,verification_token=? where user_id=? limit 1";
                                        connection.query(sql, [fbId, fbName, phoneNo, fbmail, hash, otp, responseMail[0].user_id], function(err, result) {
                                            var response1 = {
                                                "error": 'Please enter OTP',
                                                "flag": 2,
                                                "popup": updateAppPopUp,
                                                "phone_no": responseMail[0].phone_no
                                            };
                                            logging.logResponse(response1);
                                            res.send(JSON.stringify(response1));
                                        });

                                        if (code_used !== "") {
                                            var user_data = {
                                                user_id: responseMail[0].user_id,
                                                user_email: fbmail,
                                                user_name: fbName,
                                                date_registered: new Date()
                                            };
                                            user.use_code_during_login(user_data, code_used);
                                            console.log("USING CODE DURING LOGIN WHEN UPDATING THE ENTRIES IN FB REGISTRATION");
                                        }
                                    });
                                }
                            }
                        } else {
                            if ((phoneNo == "") && (responseMail.length == 0)) {
                                //checkAppVersion(responseMail[0].user_id, appVersion, deviceType, function(updateAppPopUp)
                                //{
                                var response = {
                                    "error": 'Please enter details',
                                    "flag": 3,
                                    "popup": 0
                                };
                                logging.logResponse(response);
                                res.send(JSON.stringify(response));
                                // });
                            } else if ((phoneNo != "") && (responseMail.length == 0)) {
                                isPhoneNumberUnique(phoneNo, function(isUnique) {
                                    if (isUnique === true) {
                                        var loginTime = new Date();
                                        var accessToken = utils.encrypt(fbId);
                                        var userPicture = "http://graph.facebook.com/" + fbId + "/picture?width=160&height=160";
                                        //var otp = "4444";
                                        var otp = utils.generateRandomString();
                                        var md5 = require('MD5');
                                        var hash = md5(password);

                                        checkNounceAndCreateCustomer(fbName, fbName, nounce, function(err, result) {
                                            var braintree_customer_id = '';
                                            var creditCardObj = null;
                                            var creditCardFlag = 0;
                                            if (err) {
                                                responses.sendError(res);
                                            }
                                            if (nounce !== '') {
                                                if (result.success == false) {
                                                    var response = {
                                                        "error": result.message,
                                                        "flag": constants.responseFlags.DUPLICATE_CARD
                                                    };
                                                    logging.logResponse(response);
                                                    res.send(JSON.stringify(response));
                                                    return;
                                                }
                                                creditCardFlag = 1;
                                                braintree_customer_id = result.customer.id;
                                                creditCardObj = result.customer.creditCards[0];
                                            }


                                            if (use_otp === true) {
                                                messenger.sendOTP(fbName, phoneNo, otp);

                                                utils.generateUniqueReferralCode(fbName, function(referral_code) {
                                                    var sql = "INSERT into tb_users " +
                                                        "(device_type, user_fb_id,user_fb_name,user_device_token,reg_device_token,access_token,date_registered," +
                                                        "current_location_latitude,current_location_longitude,country," +
                                                        "os_version,device_name,app_versioncode,current_user_status," +
                                                        "user_name,user_image,phone_no,user_email,password,verification_token,referral_code,unique_device_id,unique_device_id_reg,credit_card_flag,braintree_customer_id) " +
                                                        "values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                                                    var values = [deviceType, fbId, fbName, deviceToken, deviceToken, accessToken, loginTime,
                                                        latitude, longitude, country,
                                                        osVersion, deviceName, appVersion, 0,
                                                        fbName, userPicture, phoneNo, fbmail, hash, otp, referral_code, unique_device_id, unique_device_id, creditCardFlag, braintree_customer_id
                                                    ];
                                                    connection.query(sql, values, function(err, result) {
                                                        if (err) {
                                                            var response = {
                                                                "error": 'User not registered. Please try again',
                                                                "flag": 5
                                                            };
                                                            logging.logResponse(response);
                                                            res.send(JSON.stringify(response));
                                                        } else {

                                                            if (code_used !== "") {
                                                                var user_data = {
                                                                    user_id: result.insertId,
                                                                    user_email: fbmail,
                                                                    user_name: fbName,
                                                                    date_registered: new Date()
                                                                };
                                                                user.use_code_during_login(user_data, code_used);
                                                            }

                                                            if (deviceToken !== "") {
                                                                checkDuplicacyOfUsers(deviceToken);
                                                            }

                                                            if (creditCardObj) {
                                                                var sql = "INSERT INTO `tb_user_credit_cards`(`user_id`, `card_token`, `last4_digits`, `default_card`) VALUES (?,?,?,?)";
                                                                connection.query(sql, [result.insertId, creditCardObj.token, creditCardObj.verification.creditCard.last4, 1], function(err, result) {
                                                                    logging.logDatabaseQuery("Add card on signup", err, result, null);
                                                                });
                                                            }

                                                            checkAppVersion(result.insertId, appVersion, deviceType, function(updateAppPopUp) {
                                                                var response1 = {
                                                                    "error": 'Please enter OTP',
                                                                    "flag": 2,
                                                                    "popup": updateAppPopUp,
                                                                    "phone_no": phoneNo
                                                                };
                                                                logging.logResponse(response1);
                                                                res.send(JSON.stringify(response1));
                                                            });
                                                        }
                                                    });
                                                });
                                            } else {
                                                utils.generateUniqueReferralCode(fbName, function(referral_code) {
                                                    var add_user = "INSERT into tb_users " +
                                                        "(device_type, user_fb_id,user_fb_name,user_device_token,reg_device_token,access_token," +
                                                        "date_registered,current_location_latitude,current_location_longitude,country,os_version," +
                                                        "device_name,app_versioncode,current_user_status,user_name,user_image," +
                                                        "phone_no,user_email,password,verification_token,verification_status,referral_code,unique_device_id,unique_device_id_reg,credit_card_flag,braintree_customer_id) " +
                                                        "values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                                                    var values = [deviceType, fbId, fbName, deviceToken, deviceToken, accessToken,
                                                        loginTime, latitude, longitude, country, osVersion,
                                                        deviceName, appVersion, constants.userCurrentStatus.CUSTOMER_ONLINE, fbName, userPicture,
                                                        phoneNo, fbmail, hash, otp, 1, referral_code, unique_device_id, unique_device_id, creditCardFlag, braintree_customer_id
                                                    ];
                                                    connection.query(add_user, values, function(err, result) {
                                                        if (err) {
                                                            var response = {
                                                                "error": 'User not registered. Please try again',
                                                                "flag": 5
                                                            };
                                                            logging.logResponse(response);
                                                            res.send(JSON.stringify(response));
                                                        } else {
                                                            if (code_used !== "") {
                                                                var user_data = {
                                                                    user_id: result.insertId,
                                                                    user_email: fbmail,
                                                                    user_name: fbName,
                                                                    date_registered: new Date()
                                                                };
                                                                user.use_code_during_login(user_data, code_used);
                                                            }

                                                            if (deviceToken !== "") {
                                                                checkDuplicacyOfUsers(deviceToken);
                                                            }

                                                            if (creditCardObj) {
                                                                var sql = "INSERT INTO `tb_user_credit_cards`(`user_id`, `card_token`, `last4_digits`, `default_card`) VALUES (?,?,?,?)";
                                                                connection.query(sql, [result.insertId, creditCardObj.token, creditCardObj.verification.creditCard.last4, 1], function(err, result) {
                                                                    logging.logDatabaseQuery("Add card on signup", err, result, null);
                                                                });
                                                            }

                                                            checkAppVersion(result.insertId, appVersion, deviceType, function(updateAppPopUp) {

                                                                var get_user = "SELECT `user_name`, `user_image`, `user_email`, `phone_no`, `reg_as`, `access_token`, `referral_code`, `can_change_location`, `can_schedule`, `is_available` FROM `tb_users` WHERE `user_id`=? LIMIT 1";
                                                                connection.query(get_user, [result.insertId], function(err, user) {
                                                                    var get_fare = "SELECT * FROM `tb_fare`";
                                                                    connection.query(get_fare, function(err, fare_details) {
                                                                        var response = {
                                                                            "user_data": {
                                                                                "access_token": user[0].access_token,
                                                                                "user_name": user[0].user_name,
                                                                                "user_email": user[0].user_email,
                                                                                "user_image": user[0].user_image,
                                                                                "phone_no": user[0].phone_no,
                                                                                "referral_code": user[0].referral_code,
                                                                                "can_change_location": user[0].can_change_location,
                                                                                "can_schedule": user[0].can_schedule,
                                                                                "scheduling_limit": constants.SCHEDULING_LIMIT,
                                                                                "is_available": user[0].is_available,
                                                                                "gcm_intent": constants.GCM_INTENT_FLAG,
                                                                                "nukkad_enable": constants.NUKKAD_ENABLE_FLAG,
                                                                                "nukkad_icon": constants.NUKKAD_BUTTON_ICON,
                                                                                "current_user_status": current_user_status0,
                                                                                "fare_details": fare_details,
                                                                                "exceptional_driver": user[0].reg_as
                                                                            },
                                                                            "popup": updateAppPopUp
                                                                        };
                                                                        logging.logResponse(response);
                                                                        res.send(JSON.stringify(response));
                                                                    });
                                                                });
                                                            });
                                                        }
                                                    });
                                                });
                                            }
                                        });
                                    } else {
                                        var response = {
                                            "error": "This contact number is already registered with us.",
                                            "flag": constants.responseFlags.SHOW_ERROR_MESSAGE,
                                            "popup": 0
                                        };
                                        logging.logResponse(response);
                                        res.send(JSON.stringify(response));
                                    }
                                });
                            }
                        }
                    });
                } else {


                                        if ((results[0].is_blocked == 1)) {
                        checkAppVersion(results[0].user_id, appVersion, deviceType, function(updateAppPopUp) {
                            var response = {
                                "error": 'Your account is blocked by admin.',
                                "flag": 3,
                                "popup": updateAppPopUp
                            };
                            logging.logResponse(response);
                            res.send(JSON.stringify(response));
                            return
                        });
                    }
                    if ((results[0].phone_no == "") && (phoneNo == "") && (results[0].verification_token == "")) {
                        checkAppVersion(results[0].user_id, appVersion, deviceType, function(updateAppPopUp) {
                            var response = {
                                "error": 'Please enter details',
                                "flag": 3,
                                "popup": updateAppPopUp
                            };
                            logging.logResponse(response);
                            res.send(JSON.stringify(response));
                        });
                    }
                    if ((oneTimePassword == "") && (results[0].verification_status == 0) && (results[0].verification_token != "")) {
                        checkAppVersion(results[0].user_id, appVersion, deviceType, function(updateAppPopUp) {
                            var response = {
                                "error": 'Please enter OTP',
                                "flag": 2,
                                "popup": updateAppPopUp,
                                "phone_no": results[0].phone_no
                            };
                            logging.logResponse(response);
                            res.send(JSON.stringify(response));
                        });
                    } else if ((results[0].phone_no == "") && (phoneNo != "") && (results[0].verification_token == "")) {
                        var sql = "SELECT user_id FROM tb_users WHERE user_email=? LIMIT 1";
                        connection.query(sql, [fbmail], function(err, responseMailExists) {
                            if (responseMailExists.length == 1) {
                                if (username != "") {
                                    fbmail = username + '@facebook.com';
                                } else {
                                    fbmail = fbId + '@facebook.com';
                                }
                            }

                            var md5 = require('MD5');
                            var hash = md5(password);

                            //var otp = "4444";
                            var otp = utils.generateRandomString();
                            messenger.send_otp(phoneNo, otp);

                            var sql = "UPDATE tb_users set user_fb_id=?,user_name=?,phone_no=?,user_email=?,password = ?,verification_token=? where user_id=? limit 1";
                            connection.query(sql, [fbId, fbName, phoneNo, fbmail, hash, otp, results[0].user_id], function(err, result) {
                                checkAppVersion(results[0].user_id, appVersion, deviceType, function(updateAppPopUp) {
                                    var response1 = {
                                        "error": 'Please enter OTP',
                                        "flag": 2,
                                        "popup": updateAppPopUp,
                                        "phone_no": phoneNo
                                    };
                                    logging.logResponse(response1);
                                    res.send(JSON.stringify(response1));
                                });

                                if (code_used !== "") {
                                    var user_data = {
                                        user_id: results[0].user_id,
                                        user_email: fbmail,
                                        user_name: fbName,
                                        date_registered: new Date()
                                    };
                                    user.use_code_during_login(user_data, code_used);
                                    console.log("USING CODE DURING LOGIN WHEN UPDATING THE ENTRIES IN FB REGISTRATION 2");
                                }
                            });
                        });
                    } else if ((oneTimePassword != "") && (results[0].verification_status == 0)) // Verify OTP and login first time
                    {
                        if (oneTimePassword == results[0].verification_token) {
                            var userId1 = results[0].user_id;
                            var sql = "UPDATE tb_users set user_fb_id=?,verification_status=? where user_id=? limit 1";
                            connection.query(sql, [fbId, 1, userId1], function(err, result) {});

                            updateUserParams(deviceToken, latitude, longitude, country, osVersion, deviceName, deviceType, userId1);
                            checkAppVersion(userId1, appVersion, deviceType, function(updateAppPopUp) {
                                var sql = "SELECT `user_name`,`phone_no`,`access_token`,`user_image`, `reg_as`, `user_email`,`referral_code`,`current_user_status`,`can_change_location`,`can_schedule`,`is_available` FROM `tb_users` where `user_id`=?  LIMIT 1";
                                connection.query(sql, [userId1], function(err, responseMail) {
                                    var sql03 = "SELECT * FROM `tb_fare`";
                                    connection.query(sql03, function(err, fare_details) {
                                        var response1 = {
                                            "user_data": {
                                                "access_token": responseMail[0].access_token,
                                                "user_name": responseMail[0].user_name,
                                                "user_image": responseMail[0].user_image,
                                                "user_email": responseMail[0].user_email,
                                                "phone_no": responseMail[0].phone_no,
                                                "referral_code": responseMail[0].referral_code,
                                                "can_change_location": responseMail[0].can_change_location,
                                                "can_schedule": responseMail[0].can_schedule,
                                                "scheduling_limit": constants.SCHEDULING_LIMIT,
                                                "is_available": responseMail[0].is_available,
                                                "gcm_intent": constants.GCM_INTENT_FLAG,
                                                "nukkad_enable": constants.NUKKAD_ENABLE_FLAG,
                                                "nukkad_icon": constants.NUKKAD_BUTTON_ICON,
                                                "current_user_status": responseMail[0].current_user_status,
                                                "fare_details": fare_details,
                                                "exceptional_driver": responseMail[0].reg_as
                                            },
                                            "popup": updateAppPopUp
                                        };
                                        logging.logResponse(response1);
                                        res.send(JSON.stringify(response1));
                                    });

                                    mailer.sendWelcomeMail(email_ids);
                                });

                            });
                        } else {
                            var response1 = {
                                "error": 'Incorrect verification code',
                                "flag": 6
                            };
                            logging.logResponse(response1);
                            res.send(JSON.stringify(response1));
                        }
                    } else {
                        var userId = results[0].user_id;
                        var current_user_status2 = 2;
                        var date3 = new Date();
                        if (results[0].reg_as == 0) { // If register as customer....then login as customer

                            var randomString = fbmail + utils.generateRandomString();
                            var newAccessToken = utils.encrypt(randomString);

                            if (results[0].user_fb_id == "") {
                                var sql = "UPDATE `tb_users` set `user_fb_id`=? ,`current_user_status`=?,`access_token`=? where `user_id`=? limit 1";
                                connection.query(sql, [fbId, current_user_status2, newAccessToken, results[0].user_id], function(err, result) {});
                            } else {
                                var sql = "UPDATE `tb_users` set `current_user_status`=?,`access_token`=? where `user_id`=? limit 1";
                                connection.query(sql, [current_user_status2, newAccessToken, results[0].user_id], function(err, result) {});
                            }
                            var sql = "SELECT `user_name`,`phone_no`,`access_token`,`user_image`, `user_email`, `reg_as`, `referral_code`,`current_user_status`,`user_device_token`,`can_change_location`,`can_schedule`,`is_available` FROM `tb_users` where user_id=? LIMIT 1";
                            connection.query(sql, [userId], function(err, responseMail) {
                                logging.logDatabaseQuery("Getting the information for customer", err, responseMail);

                                updateUserParams(deviceToken, latitude, longitude, country, osVersion, deviceName, deviceType, userId);
                                checkAppVersion(userId, appVersion, deviceType, function(updateAppPopUp) {
                                    var sql03 = "SELECT * FROM `tb_fare`";
                                    connection.query(sql03, function(err, fare_details) {
                                        var response1 = {
                                            "user_data": {
                                                "access_token": newAccessToken,
                                                "user_name": responseMail[0].user_name,
                                                "user_image": responseMail[0].user_image,
                                                "user_email": responseMail[0].user_email,
                                                "phone_no": responseMail[0].phone_no,
                                                "referral_code": responseMail[0].referral_code,
                                                "can_change_location": responseMail[0].can_change_location,
                                                "can_schedule": responseMail[0].can_schedule,
                                                "scheduling_limit": constants.SCHEDULING_LIMIT,
                                                "gcm_intent": constants.GCM_INTENT_FLAG,
                                                "nukkad_enable": constants.NUKKAD_ENABLE_FLAG,
                                                "nukkad_icon": constants.NUKKAD_BUTTON_ICON,
                                                "current_user_status": current_user_status2,
                                                "fare_details": fare_details,
                                                "exceptional_driver": responseMail[0].reg_as
                                            },
                                            "popup": updateAppPopUp
                                        };
                                        logging.logResponse(response1);
                                        res.send(JSON.stringify(response1));
                                    });
                                });
                            });
                        } else {
                            var response = {
                                "error": "You are not registered as customer",
                                "flag": constants.responseFlags.DRIVER_LOGGING_IN
                            };
                            logging.logResponse(response);
                            res.send(JSON.stringify(response));
                        }
                    }
                }
            }
        });
    }
};


function checkFbUser(userFbId, fbmail, fbAccessToken, callback) {
    var request = require('request');
    var urls = "https://graph.facebook.com/" + userFbId + "?fields=updated_time&access_token=" + fbAccessToken;
    request(urls, function(error, response, body) {
        if (!error && response.statusCode == 200) {
            var output = JSON.parse(body);
            if (output['error']) {
                return callback(1);
            } else {
                var sql = "SELECT user_id,user_fb_id,access_token,phone_no,referral_code,verification_token,verification_status,user_name,user_image,current_user_status,reg_as,can_change_location,can_schedule,is_available,is_blocked " +
                    "FROM tb_users WHERE user_fb_id=? LIMIT 1";
                connection.query(sql, [userFbId], function(err, response) {
                    logging.logDatabaseQuery("Getting information using user_fb_id", err, response, null);
                    return callback(response);
                });
            }
        } else {
            return callback(1);
        }
    });
}


function checkDuplicacyOfUsers(deviceToken) {
    var checkDuplicateUsers =
        "SELECT `user_id`, `user_name`, `user_email`, `phone_no`, `date_registered` " +
        "FROM `tb_users` " +
        "WHERE `reg_device_token` = ?";
    connection.query(checkDuplicateUsers, [deviceToken], function(err, users) {
        if (err) {
            logging.logDatabaseQueryError("Error when checking for duplicate users", err, users);
        }

        // block users with more than 2 registrations
        if (users.length > 2) {
            var blockDuplicateQuery =
                "UPDATE `tb_users` " +
                "SET `is_blocked` = 1, `reason` = \"More than 2 registrations from device\" " +
                "WHERE `reg_device_token` = ? " +
                "ORDER BY `user_id` DESC LIMIT 1";
            console.log(blockDuplicateQuery)
            connection.query(blockDuplicateQuery, [deviceToken], function(err, users) {
                if (err) {
                    logging.logDatabaseQueryError("Error when blocking duplicate users", err, users);
                }
            });
        }
    });
}


function isPhoneNumberUnique(phone_no, callback) {
    var check_existing_user = "SELECT `user_id` FROM `tb_users` WHERE `phone_no`=?";
    connection.query(check_existing_user, [phone_no], function(err, result) {
        logging.logDatabaseQuery("Checking for phone number", err, result);
        if (result.length > 0) {
            console.log("phone number already exists");
            callback(false);
        } else {
            console.log("phone number is new");
            callback(true);
        }
    });
}


/*
 * -----------------------------------------------------------------------------
 * Check whether update popup has to be shown or not
 * INPUT : userId, version
 * OUTPUT : popup shown for update
 * -----------------------------------------------------------------------------
 */
function checkAppVersion(userId, userAppVersion, deviceType, callback) {
    var getVersionInfo =
        'SELECT `id`, `current_ios_version`, `current_android_version`, `last_force_android_version`, `last_force_ios_version`, `current_ios_is_force`, `current_android_is_force` ' +
        'FROM `tb_version` LIMIT 1';
    connection.query(getVersionInfo, function(err, result) {
        logging.logDatabaseQuery('Getting the version information for the app', err, result);

        var updateAppVersion =
            'UPDATE `tb_users` ' +
            'SET `app_versioncode` = ? ' +
            'WHERE `user_id`=? LIMIT 1';
        connection.query(updateAppVersion, [userAppVersion, userId], function(err, result) {});

        if (result.length > 0) {
            var popup = {
                title: 'Update Version',
                text: 'Update app with new version!'
            };
            if (deviceType == 0) { // android
                if (userAppVersion < result[0].current_android_version) {
                    popup.cur_version = result[0].current_android_version;
                    if (result[0].current_android_is_force == 1) {
                        popup.is_force = 1;
                    } else if (userAppVersion < result[0].last_force_android_version) {
                        popup.is_force = 1;
                    } else {
                        popup.is_force = 0;
                    }
                    return callback(popup);
                } else {
                    return callback(0);
                }
            } else { // iOS
                if (userAppVersion < result[0].current_ios_version) {
                    popup.cur_version = result[0].current_ios_version;
                    if (result[0].current_ios_is_force == 1) {
                        popup.is_force = 1;
                    } else if (userAppVersion < result[0].last_force_ios_version) {
                        popup.is_force = 1;
                    } else {
                        popup.is_force = 0;
                    }
                    return callback(popup);
                } else {
                    return callback(0);
                }
            }
        }
    });

}


exports.access_token_login = function(req, res) {
    logging.startSection("access_token_login");
    logging.logRequest(req);

    accessTokenLogin(req, function(response) {
        logging.logResponse(response);
        res.send(JSON.stringify(response));
    });
};


function accessTokenLogin(req, callback) {
    var access_token = req.body.access_token;
    var device_token = req.body.device_token;
    var latitude = req.body.latitude;
    var longitude = req.body.longitude;
    var appVersion = req.body.app_version;
    var deviceType = req.body.device_type;

    var manvalues = [access_token, deviceType];
    var checkdata = utils.checkBlank(manvalues);
    if (checkdata == 1) {
        var response = {
            "error": "some parameter missing",
            "flag": 0
        };
        callback(response);
    } else {
        var sql = "SELECT `user_id`,`user_name`,`user_email`,`reg_as`,`phone_no`,`user_image`,`referral_code`,`current_user_status`,`can_change_location`,`can_schedule`,`is_available` FROM `tb_users` WHERE `access_token`=? LIMIT 1";
        connection.query(sql, [access_token], function(err, result) {
            logging.logDatabaseQuery("Authenticating the user using the access token", err, result, null);
            if (result.length == 0) {
                var response = {
                    "error": "Invalid access token",
                    "flag": 1,
                    "popup": ''
                };
                callback(response);
            } else {
                checkAppVersion(result[0].user_id, appVersion, deviceType, function(updateAppPopUp) {
                    var update_user = "UPDATE `tb_users` set `user_device_token`=?,`current_location_latitude`=?,`current_location_longitude`=?,`device_type`=? WHERE `user_id`=? LIMIT 1 ";
                    connection.query(update_user, [device_token, latitude, longitude, deviceType, result[0].user_id], function(err, result) {
                        logging.logDatabaseQuery("Updating the information for the user", err, result, null);
                    });
                    var sql03 = "SELECT * FROM `tb_fare`";
                    connection.query(sql03, function(err, fare_details) {
                        var response = {
                            "user_data": {
                                "user_email": result[0].user_email,
                                "user_name": result[0].user_name,
                                "user_image": result[0].user_image,
                                "phone_no": result[0].phone_no,
                                "current_user_status": result[0].current_user_status,
                                "access_token": access_token,
                                "referral_code": result[0].referral_code,
                                "can_change_location": result[0].can_change_location,
                                "can_schedule": result[0].can_schedule,
                                "scheduling_limit": constants.SCHEDULING_LIMIT,
                                "is_available": result[0].is_available,
                                "gcm_intent": constants.GCM_INTENT_FLAG,
                                "nukkad_enable": constants.NUKKAD_ENABLE_FLAG,
                                "nukkad_icon": constants.NUKKAD_BUTTON_ICON,
                                "fare_details": fare_details,
                                "exceptional_driver": result[0].reg_as
                            },
                            "popup": updateAppPopUp
                        };
                        callback(response);
                    });
                });
            }
        });
    }
};


exports.email_login = function(req, res) {
    logging.startSection("email_login");
    logging.logRequest(req);

    var email = req.body.email;
    var password = req.body.password;
    var latitude = req.body.latitude;
    var longitude = req.body.longitude;
    var country = req.body.country;
    var os_version = req.body.os_version;
    var device_name = req.body.device_name;
    var appVersion = req.body.app_version;
    var deviceToken = req.body.device_token;
    var deviceType = req.body.device_type;
    var manValues = [email, password, latitude, longitude, deviceType];
    var checkdata = utils.checkBlank(manValues);
    if (checkdata == 1) {
        var response1 = {
            "error": 'some parameter missing',
            "flag": 0
        };
        logging.logResponse(response1);
        res.send(JSON.stringify(response1));
    } else {
        var md5 = require('MD5');
        var hash = md5(password);
        var sql = "SELECT `user_id`,`user_email`,is_blocked FROM `tb_users` WHERE `user_email`=? LIMIT 1";
        connection.query(sql, [email], function(err, result) {

            if (result.length == 0) {
                response1 = {
                    "error": "Email not registered. Please sign up to login",
                    "flag": 1
                };
                logging.logResponse(response1);
                res.send(response1);
            } else  if (result[0].is_blocked == 1) {
                response1 = {
                    "error": "Your account is blocked by admin. ",
                    "flag": 1
                };
                logging.logResponse(response1);
                res.send(response1);
            } else {
                var user = result[0].user_id;

                var sql = "SELECT `access_token`,`user_name`,`user_email`,`user_image`,`phone_no`,`referral_code`,`current_user_status`,`verification_status`,`reg_as`,`can_change_location`,`can_schedule`,`is_available` " +
                    "FROM `tb_users` WHERE `user_id`=? && `password`=? LIMIT 1";
                connection.query(sql, [user, hash], function(err, result_user) {
                    checkAppVersion(result[0].user_id, appVersion, deviceType, function(updateAppPopUp) {
                        var result_user_length = result_user.length
                        if (result_user_length == 0) {
                            response1 = {
                                "error": "Incorrect password",
                                "flag": 2,
                                "popup": updateAppPopUp
                            }
                            logging.logResponse(response1);
                            res.send(response1);
                        } else {
                            if (result_user[0].verification_status == 1) {
                                var date4 = new Date();
                                if (result_user[0].reg_as == 0) {
                                    var current_user_status = 2;
                                    var randomString = email + utils.generateRandomString();
                                    var newAccessToken = utils.encrypt(randomString);
                                    var sql2 = "UPDATE `tb_users` SET `current_user_status`=?,`access_token`=? WHERE `user_id`=? LIMIT 1";
                                    connection.query(sql2, [current_user_status, newAccessToken, user], function(err, result2) {

                                    });
                                    updateUserParams(deviceToken, latitude, longitude, country, os_version, device_name, deviceType, user);
                                    var sql03 = "SELECT * FROM `tb_fare`";
                                    connection.query(sql03, function(err, fare_details) {
                                        var response1 = {
                                            "user_data": {
                                                "access_token": newAccessToken,
                                                "user_email": result_user[0].user_email,
                                                "user_name": result_user[0].user_name,
                                                "user_image": result_user[0].user_image,
                                                "phone_no": result_user[0].phone_no,
                                                "referral_code": result_user[0].referral_code,
                                                "can_change_location": result_user[0].can_change_location,
                                                "can_schedule": result_user[0].can_schedule,
                                                "scheduling_limit": constants.SCHEDULING_LIMIT,
                                                "is_available": result_user[0].is_available,
                                                "gcm_intent": constants.GCM_INTENT_FLAG,
                                                "nukkad_enable": constants.NUKKAD_ENABLE_FLAG,
                                                "nukkad_icon": constants.NUKKAD_BUTTON_ICON,
                                                "current_user_status": current_user_status,
                                                "fare_details": fare_details,
                                                "exceptional_driver": result_user[0].reg_as
                                            },
                                            "popup": updateAppPopUp
                                        };
                                        logging.logResponse(response1);
                                        res.send(JSON.stringify(response1));
                                    });
                                } else {
                                    var response1 = {
                                        "error": 'You are not registered as customer',
                                        "flag": constants.responseFlags.DRIVER_LOGGING_IN
                                    };
                                    logging.logResponse(response1);
                                    res.send(JSON.stringify(response1));
                                }

                            } else {
                                var response1 = {
                                    "error": 'Please enter OTP',
                                    "flag": 3,
                                    "popup": updateAppPopUp,
                                    "phone_no": result_user[0].phone_no
                                };
                                logging.logResponse(response1);
                                res.send(JSON.stringify(response1));
                            }

                        }
                    });
                });
            }

        });
    }
};

exports.get_driver_details = function(req, res) {
    var access_token = req.body.access_token
    var driver_id = req.body.driver_id
    var manvalues = [access_token, driver_id]
    var checkblank = utils.checkBlank(manvalues)
    if (checkblank == 1) {
        var response1 = {
            "error": 'some parameter missing',
            "flag": 0
        };
        logging.logResponse(response1);
        res.send(JSON.stringify(response1));
    } else {
        utils.authenticateUser(access_token, function(result) {

            if (result == 0) {
                var response1 = {
                    "error": 'Invalid access token',
                    "flag": 1
                };
                logging.logResponse(response1);
                res.send(JSON.stringify(response1));
            } else {
                var sql = "SELECT `user_name`,`current_user_status`,`phone_no`,`user_image`,`driver_car_image`,`current_location_latitude`,`current_location_longitude`,`total_rating_got_driver`,`total_rating_driver` FROM `tb_users` WHERE `user_id`=? LIMIT 1"
                connection.query(sql, [driver_id], function(err, result_driver) {
                    if (result_driver[0].current_user_status == 1) {
                        var rating = 3;
                        if (result_driver[0].total_rating_got_driver != 0) {
                            rating = result_driver[0].total_rating_driver / result_driver[0].total_rating_got_driver;
                            rating.toString();
                            rating = rating.toFixed(1);
                        }
                        response1 = {
                            "user_name": result_driver[0].user_name,
                            "phone_no": result_driver[0].phone_no,
                            "driver_car_image": result_driver[0].driver_car_image,
                            "user_image": result_driver[0].user_image,
                            "latitude": result_driver[0].current_location_latitude,
                            "longitude": result_driver[0].current_location_longitude,
                            "rating": rating
                        }
                        var final = {
                            "driver_data": response1
                        }
                        logging.logResponse(final);
                        res.send(JSON.stringify(final));
                    } else if (result_driver[0].current_user_status == 0 || result_driver[0].current_user_status == 2) {
                        response1 = {
                            "error": "This driver is not registered with " + config.get('projectName') + "",
                            "flag": 2
                        }
                        logging.logResponse(response1);
                        res.send(JSON.stringify(response1));
                    }
                });
            }
        });
    }
};

exports.update_driver_location = function(req, res) {
    //logging.startSection("update_driver_location");
    //logging.logRequest(req);

    var access_token = req.body.access_token;
    var latitude = req.body.latitude;
    var longitude = req.body.longitude;
    var deviceToken = req.body.device_token;
    var manvalues = [access_token, latitude, longitude, deviceToken];
    var checkblank = utils.checkBlank(manvalues);

    if (checkblank == 1) {
        var response1 = {
            "error": 'some parameter missing',
            "flag": 0
        };
        //logging.logResponse(response1);
        res.send(JSON.stringify(response1));
    } else {
        utils.authenticateUser(access_token, function(result) {

            if (result == 0) {
                var response1 = {
                    "error": 'Invalid access token',
                    "flag": 1
                };
                //logging.logResponse(response1);
                res.send(JSON.stringify(response1));
            } else {
                var user_id = result[0].user_id;
                var user_status = [constants.userRegistrationStatus.DRIVER];
                var sql = "UPDATE `tb_users` " +
                    "SET `current_location_latitude`=?,`current_location_longitude`=?,`user_device_token`=? " +
                    "WHERE `user_id`=? && `current_user_status`=? && `reg_as` IN (" + user_status.toString() + ")";
                connection.query(sql, [latitude, longitude, deviceToken, user_id, 1], function(err, result_update) {
                    if (result_update.affectedRows > 0) {
                        var sql2 = "INSERT INTO `tb_updated_user_location`(`user_id`,`latitude`,`longitude`) VALUES(?,?,?)";
                        connection.query(sql2, [user_id, latitude, longitude], function(err, result2) {});

                    }
                    var response1 = {
                        "log": "Updated"
                    };
                    //logging.logResponse(response1);
                    res.send(response1);

                });
            }
        });
    }
};

exports.register_a_user = function(req, res) {
    logging.startSection("register_a_user");
    logging.logRequest(req);

    var username = req.body.user_name;
    var ph_no = req.body.ph_no;
    var email = req.body.email;
    var latitude = req.body.latitude;
    var longitude = req.body.longitude;
    var deviceToken = req.body.device_token;
    var deviceType = req.body.device_type;
    var password = req.body.password;
    var otp = req.body.otp;
    var device_name = req.body.device_name;
    var osVersion = req.body.os_version;
    var country = req.body.country;
    var appVersion = req.body.app_version;
    var code_used = req.body.referral_code;
    var unique_device_id = req.body.unique_device_id;
    var driver_paypal_id = req.body.driver_paypal_id;

    var nounce = req.body.nounce;
    var makeMeDriverFlag = req.body.make_me_driver_flag; // 0 - customer / 1-both
    var imageFlag = req.body.image_flag;

    code_used = code_used ? code_used.toUpperCase() : "";

    if (typeof nounce === 'undefined') {
        nounce = '';
    }
    if (typeof unique_device_id === 'undefined') {
        unique_device_id = 'Not sent, app version: ' + appVersion;
    }
    if (typeof driver_paypal_id === 'undefined') {
        driver_paypal_id = '';
    } else if (unique_device_id === 'not_found') {
        unique_device_id += ', app version: ' + appVersion;
    }
    // if (makeMeDriverFlag == 0) { // Register as customer
    //     
    // } else { // Register as driver
    //   
    // }

     manValues = [email, otp, deviceType,makeMeDriverFlag];

    var flag = 0;

    var email_ids = [];
    email_ids.push(email);

    // When not using OTP, the result of checkData will always be 1, hence, we can skip this part
    var checkData = utils.checkBlank(manValues);

    if (checkData == 0) {
        var sql = "SELECT `user_id`,`phone_no`,`verification_token`,`referral_code`,`verification_status`,`current_user_status`,`user_device_token` FROM `tb_users` WHERE user_email=? LIMIT 1";
        connection.query(sql, [email], function(err, response) {
            logging.logDatabaseQuery("getting information using email ID", err, response, null);
            if (err) {} else if (response.length > 0) {
                if ((otp == "") && (response[0].verification_status == 0) && (response[0].verification_token != "")) {
                    checkAppVersion(response[0].user_id, appVersion, deviceType, function(updateAppPopUp) {
                        response1 = {
                            "error": 'Please enter OTP',
                            "flag": 0,
                            "popup": updateAppPopUp,
                            "phone_no": response[0].phone_no
                        };
                        logging.logResponse(response1);
                        res.send(JSON.stringify(response1));
                    });
                } else if ((otp != "") && (response[0].verification_status == 0)) {
                    if (response[0].verification_token == otp) {
                        if (makeMeDriverFlag == 0) { // customer first time login
                            updateUserParams(deviceToken, latitude, longitude, country, osVersion, device_name, deviceType, response[0].user_id);
                            var sql = "UPDATE `tb_users` set `verification_status`=?, `current_user_status`=? WHERE user_id=? LIMIT 1";
                            connection.query(sql, [1, 2, response[0].user_id], function(err, responseUpdate) {


                                var sql = "SELECT `user_name`,`user_email`,`user_image`,`phone_no`,`current_user_status`,`referral_code`,`access_token`,`can_change_location`,`can_schedule`,`is_available` FROM `tb_users` WHERE `user_id`=? LIMIT 1";
                                connection.query(sql, [response[0].user_id], function(err, user) {
                                    logging.logDatabaseQuery("CASE 1: Getting the information for the user when the OTP is entered", err, user, null);
                                    checkAppVersion(response[0].user_id, appVersion, deviceType, function(updateAppPopUp) {
                                        var sql_fare_1 = "SELECT * FROM `tb_fare`";
                                        connection.query(sql_fare_1, function(err, fare_details) {
                                            var response1 = {
                                                "user_data": {
                                                    "user_email": user[0].user_email,
                                                    "user_name": user[0].user_name,
                                                    "user_image": user[0].user_image,
                                                    "phone_no": user[0].phone_no,
                                                    "current_user_status": 2,
                                                    "access_token": user[0].access_token,
                                                    "referral_code": user[0].referral_code,
                                                    "can_change_location": user[0].can_change_location,
                                                    "can_schedule": user[0].can_schedule,
                                                    "scheduling_limit": constants.SCHEDULING_LIMIT,
                                                    "is_available": user[0].is_available,
                                                    "gcm_intent": constants.GCM_INTENT_FLAG,
                                                    "nukkad_enable": constants.NUKKAD_ENABLE_FLAG,
                                                    "nukkad_icon": constants.NUKKAD_BUTTON_ICON,
                                                    "fare_details": fare_details
                                                },
                                                "popup": updateAppPopUp
                                            };
                                            logging.logResponse(response1);
                                            res.send(response1);
                                        });
                                    });
                                });

                            });

                            mailer.sendWelcomeMail(email_ids);
                        } else {
                            var sql1 = "UPDATE `tb_users` set `verification_status`=? WHERE user_id=? LIMIT 1";
                            connection.query(sql1, [1, response[0].user_id], function(err, responseUpdate) {
                                response1 = {

                                    "error":  'Please wait for your driver account activation by admin', "flag": constants.responseFlags.SHOW_MESSAGE};
                                logging.logResponse(response1);
                                res.send(response1);
                            });

                        }
                    } else {
                        response1 = {
                            "error": 'Incorrect verification code',
                            "flag": 1
                        };
                        logging.logResponse(response1);
                        res.send(JSON.stringify(response1));
                    }
                } else {
                var response1 = {
                    "error": 'You are already verified please login.',
                    "flag": 1
                };
                logging.logResponse(response1);
                res.send(JSON.stringify(response1));


                }



            } else {
                // This is to fix the confirmation code dialog bug for the users that couldn't be registerd because of non-unique phone number and email
                var response1 = {
                    "error": 'Incorrect verification code',
                    "flag": 1
                };
                logging.logResponse(response1);
                res.send(JSON.stringify(response1));
            }
        });
    } else {
        var sql = "SELECT `user_id`,`phone_no`,`user_email`,`verification_status` FROM `tb_users` WHERE `user_email`=? LIMIT 1";
        connection.query(sql, [email], function(err, result_check) {
            logging.logDatabaseQuery("Getting the information for the user using the user email ID", err, result_check, null);

            console.log("Is any user registered using this email ID earlier: " + result_check.length ? "yes" : "no");

            if (result_check.length == 1) {
                checkAppVersion(result_check[0].user_id, appVersion, deviceType, function(updateAppPopUp) {
                    if (result_check[0].user_id != "" && result_check[0].verification_status == 1) {
                        var response1 = {
                            "error": "This email is already registered with us",
                            "flag": 2,
                            "popup": updateAppPopUp
                        };
                        logging.logResponse(response1);
                        res.send(JSON.stringify(response1));
                    } else {
                        var response1 = {
                            "error": "Please enter OTP",
                            "flag": 0,
                            "popup": updateAppPopUp,
                            "phone_no": result_check[0].phone_no
                        };
                        logging.logResponse(response1);
                        res.send(JSON.stringify(response1));
                    }
                });
            } else {
                isPhoneNumberUnique(ph_no, function(isUnique) {
                    if (isUnique === true) {
                        //var otp = "4444";
                        var otp = utils.generateRandomString();
                        var md5 = require('MD5');
                        var hash = md5(password);
                        var accessToken = utils.encrypt(email);
                        var loginTime = new Date();
                        var userImage = config.get('s3BucketCredentials.s3URL') + '/' + config.get('s3BucketCredentials.folder.userProfileImages') + '/' + config.get("s3BucketCredentials.folder.userProfileDefaultImage");

                        var verification_status = use_otp === true ? 0 : 1;
                        var current_user_status = use_otp === true ? constants.userCurrentStatus.OFFLINE : constants.userCurrentStatus.CUSTOMER_ONLINE;

                        utils.generateUniqueReferralCode(username, function(referral_code) {

                            checkNounceAndCreateCustomer(username, username, nounce, function(err, result) {
                                var braintree_customer_id = '';
                                var creditCardObj = null;
                                var creditCardFlag = 0;
                                if (err) {
                                    responses.sendError(res);
                                }
                                if (nounce !== '') {
                                    if (result.success == false) {
                                        var response = {
                                            "error": result.message,
                                            "flag": constants.responseFlags.DUPLICATE_CARD
                                        };
                                        logging.logResponse(response);
                                        res.send(JSON.stringify(response));
                                        return;
                                    }
                                    creditCardFlag = 1;
                                    braintree_customer_id = result.customer.id;
                                    creditCardObj = result.customer.creditCards[0];
                                }
                                var sql = "INSERT into tb_users " +
                                    "(device_type,user_email,password,user_device_token,reg_device_token,access_token,date_registered," +
                                    "current_location_latitude,current_location_longitude,country,os_version,device_name,app_versioncode," +
                                    "current_user_status,user_name,user_image,phone_no,verification_token,verification_status,referral_code," +
                                    "unique_device_id,unique_device_id_reg,make_me_driver_flag,credit_card_flag,braintree_customer_id,driver_paypal_id) " +
                                    "values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                                var values = [deviceType, email, hash, deviceToken, deviceToken, accessToken, loginTime,
                                    latitude, longitude, country, osVersion, device_name, appVersion,
                                    current_user_status, username, userImage, ph_no, otp, verification_status,
                                    referral_code, unique_device_id, unique_device_id, makeMeDriverFlag, creditCardFlag,
                                    braintree_customer_id, driver_paypal_id
                                ];
                                connection.query(sql, values, function(err, result) {
                                    console.log(err);
                                    if (!err) {

                                        if (creditCardObj) {
                                            var sql = "INSERT INTO `tb_user_credit_cards`(`user_id`, `card_token`, `last4_digits`, `default_card`) VALUES (?,?,?,?)";
                                            connection.query(sql, [result.insertId, creditCardObj.token, creditCardObj.verification.creditCard.last4, 1], function(err, result) {
                                                logging.logDatabaseQuery("Add card on signup", err, result, null);
                                            });
                                        }


                                        if (imageFlag == 1) {
                                            var randomText = utils.generateRandomString()
                                            req.files.image.name = randomText + "-" + req.files.image.name;
                                            utils.uploadImageFileToS3Bucket(req.files.image, config.get('s3BucketCredentials.folder.userProfileImages'), function(result_image) {
                                                if (result_image != 0) {
                                                    var newUserImage = config.get('s3BucketCredentials.s3URL') + config.get('s3BucketCredentials.folder.userProfileImages') + '/' + result_image;
                                                    var sql = "UPDATE `tb_users` SET `user_image`=? WHERE `user_id`=? LIMIT 1";
                                                    connection.query(sql, [newUserImage, userId], function(err, result) {});
                                                }
                                            });
                                        }
                                        logging.logDatabaseQuery("The user has been entered in the database as OFFLINE", err, result, null);
                                    }

                                    if (code_used !== "") {
                                        var user_data = {
                                            user_id: result.insertId,
                                            user_email: email,
                                            user_name: username,
                                            date_registered: new Date()
                                        };
                                        user.use_code_during_login(user_data, code_used);
                                    }

                                    if (deviceToken !== "") {
                                        checkDuplicacyOfUsers(deviceToken);
                                    }

                                    if (use_otp === true) {
                                        messenger.sendOTP(username, ph_no, otp);

                                        checkAppVersion(result.insertId, appVersion, deviceType, function(updateAppPopUp) {
                                            var response1 = {
                                                "error": "Please enter OTP",
                                                "flag": 0,
                                                "popup": updateAppPopUp,
                                                "phone_no": ph_no
                                            };
                                            logging.logResponse(response1);
                                            res.send(JSON.stringify(response1));
                                        });
                                    }
                                    //                                else {
                                    //                                    // Send the response being sent earlier when the OTP for the user is verified
                                    //                                    var sql = "SELECT `user_id`, `user_name`,`user_image`,`phone_no`,`referral_code`,`current_user_status`,`access_token`,`can_change_location`,`can_schedule`,`is_available` FROM `tb_users` WHERE user_email=? LIMIT 1";
                                    //                                    connection.query(sql, [email], function (err, user) {
                                    //                                        logging.logDatabaseQuery("getting information using email ID", err, user, null);
                                    //
                                    //                                        checkAppVersion(user[0].user_id, appVersion, deviceType, function (updateAppPopUp) {
                                    //
                                    //                                            var get_fare = "SELECT * FROM `tb_fare` WHERE `id`=? LIMIT 1";
                                    //                                            connection.query(get_fare, [1], function (err, fare_details) {
                                    //                                                var response = {
                                    //                                                    "user_data": {
                                    //                                                        "user_name": user[0].user_name,
                                    //                                                        "user_image": user[0].user_image,
                                    //                                                        "phone_no": user[0].phone_no,
                                    //                                                        "current_user_status": constants.userCurrentStatus.CUSTOMER_ONLINE,
                                    //                                                        "access_token": user[0].access_token,
                                    //                                                        "referral_code": user[0].referral_code,
                                    //                                                        "can_change_location": user[0].can_change_location,
                                    //                                                        "can_schedule": user[0].can_schedule,
                                    //                                                        "scheduling_limit" : constants.SCHEDULING_LIMIT,
                                    //                                                        "is_available" : user[0].is_available,
                                    //                                                        "gcm_intent" : constants.GCM_INTENT_FLAG,
                                    //                                                        "nukkad_enable" : constants.NUKKAD_ENABLE_FLAG,
                                    //                                                        "nukkad_icon" : constants.NUKKAD_BUTTON_ICON,
                                    //                                                        "fare_details": fare_details
                                    //                                                    },
                                    //                                                    "popup": updateAppPopUp
                                    //                                                };
                                    //                                                logging.logResponse(response);
                                    //                                                res.send(response);
                                    //                                            });
                                    //                                        });
                                    //                                    });
                                    //                                }
                                });


                            });
                        });
                    } else {
                        var response = {
                            "error": "This contact number is already registered with us.",
                            "flag": 2,
                            "popup": 0
                        };
                        logging.logResponse(response);
                        res.send(JSON.stringify(response));
                    }
                });
            }
        });
    }
};


function checkNounceAndCreateCustomer(username, username, nounce, callback) {
    if (nounce) {

        make_payments.createCustomer(username, username, nounce, function(err, result) {
            callback(err, result);

        });

    } else {
        callback(null, 1)
    }
}

exports.logout = function(req, res) {
    var access_token = req.body.access_token
    var check_blank = [access_token]
    var checkdata = utils.checkBlank(check_blank)
    if (checkdata == 1) {
        var response1 = {
            "error": "some parameter missing",
            "flag": 0
        }
        logging.logResponse(response1);
        res.send(JSON.stringify(response1));
    } else {
        utils.authenticateUser(access_token, function(result) {
            if (result == 0) {
                var response1 = {
                    "error": "Invalid access token",
                    "flag": 1
                }
                logging.logResponse(response1);
                res.send(response1);
            } else {
                var user_id = result[0].user_id;
                var sql = "UPDATE `tb_users` SET `current_user_status`=?,`user_device_token`=? WHERE `user_id`=? LIMIT 1"
                connection.query(sql, [0, '', user_id], function(err, result_update) {
                    var response1 = {
                        "log": "sucessfully logged off"
                    }
                    logging.logResponse(response1);
                    res.send(JSON.stringify(response1));
                });

            }
        });
    }
}

function updateUserParams(deviceToken, latitude, longitude, country, osVersion, deviceName, deviceType, userId) {
    var date = new Date();
    var sql = "UPDATE tb_users set user_device_token=?,current_location_latitude=?,current_location_longitude=?,country=?,os_version=?,device_name=?,device_type=?,last_login=? where user_id=? limit 1";
    connection.query(sql, [deviceToken, latitude, longitude, country, osVersion, deviceName, deviceType, date, userId], function(err, result) {});
}


exports.forgotPasswordFromEmail = function(req, res) {
    logging.startSection("forgot_password");
    logging.logRequest(req);

    var email = req.body.email;
    var manValues = [email];
    var checkData = utils.checkBlank(manValues);
    if (checkData == 1) {

        responses.parameterMissingResponse(res);
    } else {
        var sql = "SELECT user_id,user_name FROM tb_users WHERE user_email=? LIMIT 1";
        connection.query(sql, [email], function(err, response) {
            logging.logDatabaseQuery("Getting user information from email", err, response, null);

            if (response.length == 1) {
                //var password = utils.generateRandomStringForPassword();

                var password = generatePassword(6, false, /\d/);
                var md5 = require('MD5');
                var encrypted_pass = md5(password);
                // var encrypted_pass = utils.encrypt(password);
                var name = response[0].user_name;


                var game_name = config.get('projectName');
                var to = email;
                var sub = config.get('projectName') + " - Your New Password";
                var msg = "Hi, \n\n";
                msg += "You seem to have forgotten your password for your account with the email address: " + email + "\n\n";
                msg += "Your new password is: " + password + "\n\n\n";
                msg += "Thanks. \n";
                utils.sendEmailForPassword(to, msg, sub, function(result) {
                    if (result == 1) {
                        var sql = "UPDATE tb_users set password=? WHERE user_email=? LIMIT 1";
                        connection.query(sql, [encrypted_pass, email], function(err, response) {

                            var response = {
                                "log": 'New password has been sent to your email'
                            };
                            logging.logResponse(response);
                            res.send(JSON.stringify(response));
                        });
                    } else {
                        var response = {
                            "error": 'Error sending new password to your email. Please try again',
                            "flag": constants.responseFlags.SHOW_ERROR_MESSAGE
                        };
                        logging.logResponse(response);
                        res.send(JSON.stringify(response));
                    }
                });
            } else {
                var response = {
                    "error": 'This email is not registered with ' + config.get('projectName'),
                    "flag": constants.responseFlags.SHOW_ERROR_MESSAGE
                };
                logging.logResponse(response);
                res.send(JSON.stringify(response));
            }

        });
    }
};


function exceptional_driver(user_id, callback) {
    var sql = "SELECT `user_id` FROM `tb_exceptional_users` WHERE `user_id`=? LIMIT 1";
    connection.query(sql, [user_id], function(err, result) {
        if (result.length > 0) {
            return callback(1);
        } else {
            return callback(0);
        }
    });
}


exports.start_app_using_access_token = function(req, res) {

    res.header("Access-Control-Allow-Origin", "*");

    var split_fare = require('./split_fare')

    logging.startSection("start_app_using_access_token");
    logging.logRequest(req);

    var f_login = false,
        login_response = {};
    var f_status = false,
        status_response = {};
    var f_drivers = false,
        drivers_response = {};
    var f_split_fare_popup = false,
        split_fare_popup = {};
    var f_last_ride = false,
        last_ride_response = null;
    //var f_car_types = false, car_types_response = {};

    var access_token = req.body.access_token;
    var latitude = req.body.latitude;
    var longitude = req.body.longitude;
    var carType = req.body.car_type;

    if (typeof carType === 'undefined') {
        carType = 0;
    }

    utils.authenticateUser(access_token, function(result) {
        if (result != 0) {
            var user_id = result[0].user_id;
            var current_user_status = result[0].current_user_status;
            var ride_module = require('./ride_parallel_dispatcher');



            async.parallel([
                    function(callback) {

                        accessTokenLogin(req, function(response) {
                            callback(null, response)

                        });

                    },
                    function(callback) {


                        if (current_user_status === constants.userCurrentStatus.CUSTOMER_ONLINE) {

                            ride_module.getLastRideForRating(user_id, function(err, lastRide) {
                                callback(null, lastRide)

                            });
                        } else {


                            setTimeout(function() {
                                callback(null, {})
                            }, 200);


                        }

                    },
                    function(callback) {


                        ride_module.getCurrentUserStatus(user_id, current_user_status, function(response) {

                            callback(null, response)

                        });

                    },
                    function(callback) {



                        ride_module.findDriversInArea(carType, latitude, longitude, function(response) {
                            callback(null, response)


                        });


                    },
                    function(callback) {


                        split_fare.isSplitFarePopup(user_id, function(response) {

                            callback(null, response)

                        });

                    }
                ],
                // optional callback
                function(err, results) {



                    var response = {
                        login: results[0],
                        last_ride: results[1],
                        status: results[2],
                        drivers: results[3],
                        split_fare_popup: results[4]
                            //cars      : car_types_response
                    };
                    logging.logResponse(response);
                    res.send(JSON.stringify(response));
                    // the results array will equal ['one','two'] even though
                    // the second function had a shorter timeout.
                });


        } else {
            response = {
                "error": 'invalid access token',
                "flag": constants.responseFlags.INVALID_ACCESS_TOKEN
            };
            //logging.logResponse(response);
            res.send(JSON.stringify(response));
        }
    });
};


//exports.send_otp_via_call = function(req, res){
//    logging.startSection("send_otp_via_call");
//    logging.logRequest(req);
//
//    var phone_no = req.body.phone_no;
//
//    var isBlank = utils.checkBlank([phone_no]);
//    if(isBlank == 1){
//        var response = {
//            flag    : constants.responseFlags.PARAMETER_MISSING,
//            error   : "some parameter missing"};
//        logging.logResponse(response);
//        res.send(response);
//        return;
//    }
//
//    var verify_user = "SELECT `user_id`, `verification_calls` FROM `tb_users` WHERE `phone_no`=?";
//    connection.query(verify_user, [phone_no], function(err, user){
//        if(user.length === 0){
//            var response = {
//                flag: constants.responseFlags.SHOW_ERROR_MESSAGE,
//                message: "No user with contact number " + phone_no + " is currently registered with us."
//            };
//            res.send(response);
//            return;
//        }
//
//        user = user[0];
//
//        if(user.verification_calls >= 5){
//            var response = {
//                flag: constants.responseFlags.SHOW_ERROR_MESSAGE,
//                message: 'Sorry! You have already tried ' + user.verification_calls + ' times. Please contact the customer support at +91 9023-121-121 between 10 am to 7 pm from your registered mobile number to verify.'};
//            res.send(response);
//            return;
//        }
//
//        var accountSid = config.get('twillioCredentials.accountSid');
//        var authToken = config.get('twillioCredentials.authToken');
//        var client = require('twilio')(accountSid, authToken);
//
//        client.calls.create({
//            url: config.get('getOTPUsingNumberURL'),
//            to: phone_no,
//            from: config.get('twillioCredentials.fromNumber')
//        }, function(err, call) {
//            console.log(
//                "Making Twilio call for OTP:\n" +
//                "Error: " + JSON.stringify(err, undefined, 2) + "\n" +
//                "Call: " + JSON.stringify(call, undefined, 2));
//        });
//
//        var update_calls = "UPDATE `tb_users` SET `verification_calls`=`verification_calls`+1 WHERE `user_id`=?";
//        connection.query(update_calls, [user.user_id], function(err, result){
//            logging.logDatabaseQuery("Incrementing the number of verification calls", err, result);
//        });
//
//        var response = {
//            flag: constants.responseFlags.SHOW_MESSAGE,
//            message: "You will shortly receive a call with your One Time Password."
//        };
//        res.send(response);
//    });
//};



exports.get_otp_using_number = function(req, res) {
    logging.startSection("get_otp_using_number");
    logging.logRequest(req);

    var phone_no = req.body.To;

    var isBlank = utils.checkBlank([phone_no]);
    if (isBlank == 1) {
        var response = {
            "error": "some parameter missing",
            "flag": constants.responseFlags.PARAMETER_MISSING
        };
        logging.logResponse(response);
        res.send(JSON.stringify(response));
    } else {
        var get_otp = "SELECT `user_id`, `user_name`, `phone_no`, `verification_token` FROM `tb_users` WHERE `phone_no`=? ORDER BY `user_id` LIMIT 1";
        connection.query(get_otp, [phone_no], function(err, user) {
            logging.logDatabaseQuery("Getting the user registered using the phone number: " + phone_no, err, user, null);

            if (user.length > 0) {
                var otp = user[0].verification_token.toString();
                var otp_string = "";
                for (var i = 0; i < otp.length; i++) {
                    otp_string += "<Pause length=\"1\"/>" + otp[i];
                }
                otp_string += "<Pause length=\"1\"/>";
                otp_string = "You are talking to " + config.get('projectName') + " Support. This is an automated call. Your One Time Password for " + config.get('projectName') + " application is " + otp_string + ". ";
                otp_string = otp_string + otp_string;

                var response =
                    '<?xml version="1.0" encoding="UTF-8"?>' +
                    '<Response>' +
                    '<Say voice="woman">' +
                    otp_string +
                    '</Say>' +
                    '</Response>';
                logging.logResponse(response);
                res.send(response);
            } else {
                var phone_no_string = "";
                for (var i = 0; i < phone_no.length; i++) {
                    phone_no_string += "<Pause length=\"1\"/>" + phone_no[i];
                }
                phone_no_string += "<Pause length=\"1\"/>";
                var wrong_no_string = "The number " + phone_no_string + " is not registered with " + config.get('projectName') + ". Please verify your registration. This is an automated call. ";
                wrong_no_string = wrong_no_string + wrong_no_string;

                var response =
                    '<?xml version="1.0" encoding="UTF-8"?>' +
                    '<Response>' +
                    '<Say voice="woman">' +
                    wrong_no_string +
                    '</Say>' +
                    '</Response>';
                logging.logResponse(response);
                res.send(response);
            }
        });
    }
};

exports.UserProfileInformation = function(req, res) {
    logging.startSection("user_profile_information");
    logging.logRequest(req);

    var accessToken = req.body.access_token;

    var manValues = [accessToken];
    var isBlank = utils.checkBlank(manValues);
    if (isBlank == 1) {
        responses.parameterMissingResponse(res);
        return;
    }

    utils.authenticateUser(accessToken, function(user) {
        if (user != 0) {
            var response = {
                "user_name": user[0].user_name,
                "user_email": user[0].user_email,
                "user_image": user[0].user_image
            };
            res.send(response);
        } else {
            response = {
                "error": 'invalid access token',
                "flag": constants.responseFlags.INVALID_ACCESS_TOKEN
            };
            //logging.logResponse(response);
            res.send(JSON.stringify(response));
        }
    });
}

exports.editCustomerProfile = function(req, res) {
    logging.startSection("edit_customer_profile");
    logging.logRequest(req);

    var accessToken = req.body.access_token;
    var userName = req.body.user_name;
    var imageFlag = req.body.image_flag;

    var manValues = [accessToken, userName, imageFlag];
    var isBlank = utils.checkBlank(manValues);
    if (isBlank == 1) {
        responses.parameterMissingResponse(res);
        return;
    }

    utils.authenticateUser(accessToken, function(user) {
        if (user == 0) {
            responses.authenticationErrorResponse(res);
            return;
        }
        if (imageFlag == 1) {
            var randomText = utils.generateRandomString()
            req.files.image.name = randomText + "-" + req.files.image.name;
            utils.uploadImageFileToS3Bucket(req.files.image, config.get('s3BucketCredentials.folder.userProfileImages'), function(result_image) {
                if (result_image != 0) {
                    var newUserImage = config.get('s3BucketCredentials.s3URL') + '/' + config.get('s3BucketCredentials.folder.userProfileImages') + '/' + result_image;
                    var sql = "UPDATE `tb_users` SET `user_name`=?,`user_image`=? WHERE `user_id`=? LIMIT 1";
                    connection.query(sql, [userName, newUserImage, user[0].user_id], function(err, result) {
                        if (err) {
                            logging.logDatabaseQueryError("Error when editing customer profile", err, result);
                            responses.sendError(res);
                            return;
                        }
                        var response = {
                            "log": "Profile has been updated successfully",
                            "user_image": newUserImage
                        };
                        res.send(response);
                    });
                }
            });
        } else {
            var sql = "UPDATE `tb_users` SET `user_name`=? WHERE `user_id`=? LIMIT 1";
            connection.query(sql, [userName, user[0].user_id], function(err, result) {
                if (err) {
                    logging.logDatabaseQueryError("Error when editing customer profile", err, result);
                    responses.sendError(res);
                    return;
                }
                var response = {
                    "log": "Profile has been updated successfully"
                };
                res.send(response);
            });
        }

    });
}




exports.updateManualDestinationInfo = function(req, res) {
    logging.startSection("update_manual_destination");
    logging.logRequest(req);

    var accessToken = req.body.access_token;
    var engagementId = req.body.engagement_id;
    var manualDestinationLatitude = req.body.manual_destination_latitude;
    var manualDestinationLongitude = req.body.manual_destination_longitude;
    var manualDestinationAddress = req.body.manual_destination_address;


    var manValues = [accessToken, manualDestinationLatitude, manualDestinationLongitude, manualDestinationAddress];
    var isBlank = utils.checkBlank(manValues);
    if (isBlank == 1) {
        responses.parameterMissingResponse(res);
        return;
    }

    utils.authenticateUser(accessToken, function(user) {
        if (user != 0) {
            var sql = "UPDATE `tb_engagements` SET `manual_destination_latitude`=?,`manual_destination_longitude`=?,`manual_destination_address`=? WHERE `engagement_id`=? LIMIT 1";
            connection.query(sql, [manualDestinationLatitude, manualDestinationLongitude, manualDestinationAddress, engagementId], function(err, result) {
                if (err) {
                    logging.logDatabaseQueryError("Error when updating destination by customer", err, result);
                    responses.sendError(res);
                    return;
                }
                var response = {
                    "log": "Destination has been updated successfully"
                };
                res.send(response);

            });
        } else {
            response = {
                "error": 'invalid access token',
                "flag": constants.responseFlags.INVALID_ACCESS_TOKEN
            };
            //logging.logResponse(response);
            res.send(JSON.stringify(response));
        }
    });
}


exports.resetPassword = function(req, res) {
    logging.startSection("reset_password");
    logging.logRequest(req);

    var accessToken = req.body.access_token;
    var oldPassword = req.body.old_password;
    var newPassword = req.body.new_password;

    var manValues = [accessToken, oldPassword, newPassword];
    var isBlank = utils.checkBlank(manValues);
    if (isBlank == 1) {
        responses.parameterMissingResponse(res);
        return;
    }

    utils.authenticateUser(accessToken, function(user) {
        if (user != 0) {
            var md5 = require('MD5');
            oldPassword = md5(oldPassword);
            newPassword = md5(newPassword);
            //var randomString = user[0].user_email + utils.generateRandomString();
            //var newAccessToken = utils.encrypt(randomString);
            var sql = "UPDATE `tb_users` SET `password`=? WHERE `user_id`=? && `password`=? LIMIT 1";
            connection.query(sql, [newPassword, user[0].user_id, oldPassword], function(err, result) {
                if (err) {
                    logging.logDatabaseQueryError("Error in reset password", err, result);
                    responses.sendError(res);
                    return;
                }
                if (result.affectedRows == 0) {
                    var response = {
                        "error": "Wrong password",
                        "flag": constants.responseFlags.INCORRECT_PASSWORD
                    };
                    res.send(response);
                    return;
                }
                var response = {
                    "log": "Password has been updated successfully"
                };
                res.send(response);

            });
        } else {
            response = {
                "error": 'invalid access token',
                "flag": constants.responseFlags.INVALID_ACCESS_TOKEN
            };
            //logging.logResponse(response);
            res.send(JSON.stringify(response));
        }
    });
}

function getAvailableCars(callback) {
    var sql = "SELECT * FROM `tb_fare`";
    connection.query(sql, function(err, resultCars) {

        callback(resultCars);

    });
}
