var mysql = require('mysql');
var db_config = {
    host: config.get('databaseSettings.host'),
    user: config.get('databaseSettings.user'),
    password: config.get('databaseSettings.password'),
    database: config.get('databaseSettings.database'),
    port : config.get('databaseSettings.mysqlPORT'),
    connectionLimit : 50 //config.get('databaseSettings.connectionLimit')
};

var pool = mysql.createPool(db_config);

exports.query = function (query, values, callback) {

    pool.getConnection(function (err, connection) {

        if (err) {
            console.log(err);
            return callback(err,undefined);
        }
        else {

            connection.query(query, values, function (err, rows) {

                // And done with the connection.
                connection.release();

                if (err) {
                    console.log(err);
                    return callback(err,undefined);
                }
                else {
                    return callback(null,rows);
                }
                // Don't use the connection here, it has been returned to the pool.
            });
        }
        // connected! (unless `err` is set)
    });
};
