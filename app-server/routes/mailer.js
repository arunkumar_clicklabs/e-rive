/**
 * This javascript file will be used for sending mails...
 */

var logging = require('./logging');
var utils = require('./../utils/commonfunction');
var datetime_utils = require('./../utils/datetimefunctions');
var constants = require('./constants');
var async = require('async');
var fs = require('fs');


exports.sendMailForCompletedRide = function(engagement_id, customer_id, driver_id, drop_info, fare_info){
    var customer_info = "SELECT `user_id`,`user_name`,`phone_no`,`user_email`,`current_location_latitude` as latitude,`current_location_longitude` as longitude " +
        "FROM `tb_users` WHERE `user_id`= ? LIMIT 1";
    connection.query(customer_info, [customer_id], function(err, customer) {
        logging.logDatabaseQuery("Fetch information for customer", err, customer, null);

        var driver_info_query = "SELECT `user_id`,`user_name`,`phone_no`,`user_email`,`current_location_latitude` as latitude,`current_location_longitude` as longitude " +
            "FROM `tb_users` WHERE `user_id`= ? LIMIT 1";
        connection.query(driver_info_query, [driver_id], function (err, driver) {
            logging.logDatabaseQuery("Fetch information for driver", err, driver, null);

            // Send e-mail to the customer
            {
                var name = [];
                var username = customer[0].user_name;
                var name = username.split(" ");
                if (customer[0].user_email != "") {
                    var sub = "Your "+config.get('projectName')+" ride summary";
                    var msg = "Hi " + name[0] + ", \n\n";

                    msg += "Hope you had a good time in your "+config.get('projectName')+" ride and made a new friend. Here's a summary of your ride\n\n";
                    msg += "Distance: " + parseFloat(fare_info.distance).toFixed(2) + " Kms\n";
                    msg += "Ride time: " + parseFloat(fare_info.ride_time).toFixed(2) + " mins\n\n";
                    if(fare_info.coupon_used === true){
                        msg += "And since, you had a free ride available with you, your fare details are as following\n";
                        msg += "Total fare: €" + fare_info.total_fare + "\n";
                        msg += "Discount: €" + fare_info.discount + "\n";
                        msg += "Fare to be paid: €" + fare_info.to_pay + "\n\n";
                    }
                    else{
                        msg += "Total fare: €" + fare_info.total_fare + "\n\n";
                    }
                    msg += "Thank you for using "+config.get('projectName')+" to find a ride. Keep riding along! \n\n";
                    msg += config.get('emailCredentials.signature');
                    utils.sendEmailForPassword(customer[0].user_email, msg, sub, function (resultEmail) {
                    });
                }
            }
        });
    });
};



exports.sendMailForManualEngageFollowup = function(customerId, driverId){
    var customerInfo = "SELECT `user_id`,`user_name`,`phone_no`,`user_email`,`current_location_latitude`,`current_location_longitude` FROM `tb_users` WHERE `user_id`= ? LIMIT 1";
    connection.query(customerInfo, [customerId], function(err, customer)
    {
        logging.logDatabaseQuery("Fetch information for customer", err, customer, null);
        customer = customer[0];

        var driver_info_query = "SELECT `user_id`,`user_name`,`phone_no`,`user_email`,`current_location_latitude`,`current_location_longitude` FROM `tb_users` WHERE `user_id`= ?";
        connection.query(driver_info_query, [driverId], function(err, driver)
        {
            logging.logDatabaseQuery("Fetch information for driver", err, driver, null);
            driver = driver[0];

            var game_name = config.get('projectName');
            var to = config.get('emailCredentials.senderEmail');
            var sub = "FOLLOW UP REQUIRED for manual engagement between  " + driver.user_name + " and " + customer.user_name;

            var msg = "Customer summary:\n\n";
            msg +=
                "\tUser Id : "+customer.user_id+"\n" +
                "\tName : "+customer.user_name+"\n" +
                "\tPhone : "+customer.phone_no+"\n" +
                "\tEmail : "+customer.user_email+"\n"+
                "\tCurrent location: https://www.google.co.in/maps/preview?q="+customer.current_location_latitude+","+customer.current_location_longitude+"\n" +
                "\tFor iOS: comgooglemaps://?q="+customer.current_location_latitude+","+customer.current_location_longitude+ "\n\n";

            msg += "Driver summary:\n\n";
            msg +=
                "\tUser Id : "+driver.user_id+"\n" +
                "\tName : "+driver.user_name+"\n" +
                "\tPhone : "+driver.phone_no+"\n" +
                "\tEmail : "+driver.user_email+"\n"+
                "\tCurrent location: https://www.google.co.in/maps/preview?q="+driver.current_location_latitude+","+driver.current_location_longitude+"\n" +
                "\tFor iOS: comgooglemaps://?q="+driver.current_location_latitude+","+driver.current_location_longitude+ "\n\n";

            msg += config.get('emailCredentials.signature');
            utils.sendEmailForPassword(to, msg, sub, function(resultEmail) {});
        });
    });
};


exports.sendWelcomeMail = function(email_ids){
    var sub='Welcome to ' + config.get('projectName');
    var html='<!DOCTYPE html>';
    html+='<head>';
    html+='<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
    html+=' <title>Welcome</title>';
    html+='<style type="text/css">';
    html+="body{font-family: 'O=pen Sans', 'Lucida Grande', 'Segoe UI', Arial, Verdana, 'Lucida Sans Unicod=e', Tahoma, 'Sans Serif';}";
    html+='a{ text-decoration:none; color:#0093d4;}';
    html+='</style>';
    html+='</head>';
    html+='<body background="#f1f1f1" style="font-family: "O=pen Sans", "Lucida Grande", "Segoe UI", Arial, Verdana, "Lucida Sans Unicod=e", Tahoma, "Sans Serif";margin:0; padding:0;  background-color:#f1f1f1; font-size:15px; line-height:28px; color:#202020;">';
    html+='<table width="600px" border="0" cellspacing="0" cellpadding="0" align="center" background="#fff" style="background-color:#fff;">';
    html+='<tr>';
    html+='<td>';
    html+='<table width="600px" border="0" cellspacing="0" cellpadding="0" align="center" style="padding: 5px;">';
    html+='<tr>';
    html+='<td colspan="2" style="text-align: justify;"><p align="center" style="font-size: 20px; font-weight:300; line-height: 20px;color:#555; ">Welcome to '+config.get('projectName')+'<br></p>';
    html+='<img width="100%"  src='+config.get('welcomeMailImage')+'>';
    html+='<p style="font-size: 14px; padding: 0px; margin:0px;color:#202020;"><br>We are excited to have you on board with us.<br></p>';
    html+="<p style='font-size: 14px; padding: 0px; margin:0px; color:#202020;'><br>If you're looking for a comfortable yet affordable ride with is all you need "+config.get('projectName')+". Your safety and happiness is our priority. It's a reliable and quick way to travel from one destination to another.<br><br>Enjoy your ride with "+config.get('projectName')+"!</p>";
    html+='<p style="font-size: 14px; padding: 0px; margin:0px; color:#202020;"><br>Thanks,<br>The '+config.get('projectName')+' Team<br><a href="mailto:'+config.get('emailCredentials.From')+'">'+config.get('emailCredentials.From')+'</a></p>';
    html+='<table width="600px" cellspacing="0" cellpadding="0" border="0" align="center" bgcolor="" style="font-family:Arial,serif;font-size:12px;font-weight:300;border-bottom-width:1px">';
    html+='<tbody><tr>';
    html+='<td align="center" valign="center" width="25%">';
    html+='<a href= '+config.get('appStoreURL')+'>';
    html+='<img width="auto" style="padding : 5px; margin-top: 5%; margin-bottom: 5%; float: right; " src="http://i59.tinypic.com/2zfr0ug.png">';
    html+='</a>';
    html+='</td>';
    html+='<td align="center" valign="center" width="25%" style="padding:15px;">';
    html+='<a href= '+config.get('playStoreURL')+'>';
    html+='<img width="auto" style="padding : 5px; margin-top: 5%; margin-bottom: 5%; float: left;" src="http://i60.tinypic.com/30xhi4y.png">';
    html+='</a>';
    html+='</td>';
    html+='</tr>';
    html+='</tbody></table>';
    html+='<p style="font-size: 14px; padding: 0px; margin:0px; color:#202020;">You are receiving this email because you signed up for '+config.get('projectName')+'. For support requests, please reach us on <a href="mailto:" '+config.get('emailCredentials.From')+'">'+config.get('emailCredentials.From')+'</a><br></p>';
    html+='</td>';
    html+='</tr>';
    html+='</table>';
    html+='</td>';
    html+='</tr>';
    html+='</table>';
    html+='<table width="610px" border="0" cellspacing="0" cellpadding="0" align="center" background="#fff" style="background-color:#272727;">';
    html+='<tbody><tr>';
    html+='<td align="center" valign="center" width="400">';
    html+='<a href='+config.get('websiteURL')+'>';
    html+='<img src='+config.get('logo')+' style="display:table-cell;vertical-align:middle;border:none;color:#818181;font-size:9px; float: left; margin-left: 25px;">';
    html+='</a>';
    html+='</td>';
    html+='<td align="right" valign="top" width="100px" style="padding-top:15px; padding-bottom:15px; padding-left:0px; padding-right:0px;"  width="40px">';
    html+='<a style="border:0;text-decoration:none;display:table" href= '+config.get('fbPage')+' target="_blank">';
    html+='<img src="http://i62.tinypic.com/2qm0z90.png" style="float: right; margin-right: 25px; ">';
    html+='</a>';
    html+='</td>';
    html+='</tr>';
    html+='</tbody></table>';
    html+='</body>';
    html+='</html>';

    utils.sendHtmlContent(email_ids, html, sub, function(resultEmail)
    {
    });
};


exports.sendMailForUsingPromotionalCode = function(user_email, user_name, num_rides, promo_code){
    var subject = "";
    if(num_rides == 1){
        subject = "You have a FREE ride with "+config.get('projectName');
    }
    else{
        subject = "You have " + num_rides + " FREE rides with "+config.get('projectName');
    }
    var body =
        "Hey " + user_name.split(' ')[0] + ",\n\n" +
        "Awesome. You have used the promo code " + promo_code + " on "+config.get('projectName');
    if(num_rides == 1)
        body += "and you have got a free ride. ";
    else
        body += "and you have got " + num_rides + " free rides. ";
        body +=
        "Have fun in using "+config.get('projectName')+". Should you need any help, send us an email back.\n\n" ;
        body += config.get('emailCredentials.signature');
    utils.sendEmailForPassword(user_email, body, subject, function(err, result){
    });
};


exports.sendScheduleConfirmationMail = function(customer_id, pickup){
    var customer_info = "SELECT `user_id`,`user_name`,`phone_no`,`user_email` FROM `tb_users` WHERE `user_id`= ? LIMIT 1";
    connection.query(customer_info, [customer_id], function(err, customer) {
        logging.logDatabaseQuery("Fetch information for customer", err, customer, null);

        if (customer[0].user_email != '') {
            var to = customer[0].user_email;
            var sub = "A pickup scheduled with "+config.get('projectName')+"";

            var pickup_time = datetime_utils.changeTimezoneFromUtcToIst(pickup.time).replace(/T/, ' ').replace(/\..+/, '');
            var msg = "Hi " + customer[0].user_name.split(" ")[0] + ", \n\n";
            msg +=
                "You have scheduled a "+config.get('projectName')+" for " + pickup_time + " at " + pickup.address + ". " +
                "We will confirm your booking 20 minutes before the scheduled pickup time. " +
                ""+config.get('projectName')+" app is available on IOS and Android stores.\n\n";

            msg += config.get('emailCredentials.signature');

            utils.sendEmailForPassword(to, msg, sub, function (resultEmail) {
            });
        }
    });
};


exports.sendMail = function(req, res){
    var user_id = req.body.user_id;
    var subject = req.body.subject;
    var message = req.body.message;

    var customer_info = "SELECT `user_email` FROM `tb_users` WHERE `user_id`= ? LIMIT 1";
    connection.query(customer_info, [user_id], function(err, customer) {
        logging.logDatabaseQuery("Fetch information for customer", err, customer, null);

        if (customer.length > 0) {
            customer = customer[0];
            if (customer.user_email != '') {
                var to = customer.user_email;
                utils.sendEmailForPassword(to, subject, message, function(result_email){});
                res.send(JSON.stringify({log: 'e-mail sent'}));
            }
        }
        else{
            res.send(JSON.stringify({log: 'user doesn\'t exist'}));
        }
    });
};


exports.sendMailToReferringUser = function(friends_name, referring_user_id){
    var referring_user = undefined;

    function getReferringUser(callback){
        var get_user = "SELECT `user_name`, `user_email`, `user_name` FROM `tb_users` WHERE `user_id`=?";
        connection.query(get_user, [referring_user_id], function(err, user){
            logging.logDatabaseQuery("Getting the information for referring person", err, user);
            referring_user = user[0];
            callback();
        });
    }

    function sendMail(){
        var first_name = friends_name.split(' ')[0];
        var to = referring_user.user_email;
        var sub = friends_name + " has just registered on "+config.get('projectName')+" using your referral code";
        var msg = "Hey " + referring_user.user_name.split(' ')[0] +",\n\n" +
            "Awesome. " + first_name + " has just registered on "+config.get('projectName')+" as " + first_name + " was referred by you. " +
            "We are awaiting for " + first_name + " to take the first ride with "+config.get('projectName')+" which is actually free. " +
            "Once " + first_name + " takes the first ride, we will credit the referral free ride into your account. " +
            "In the meanwhile if you can help " + first_name + " in using "+config.get('projectName')+", that would be of great help.\n\n" ;
        msg += config.get('emailCredentials.signature');

        utils.sendEmailForPassword(to, msg, sub, function(result){
        });
    }

    // Get the information for the referring user and send the email
    async.series([getReferringUser], sendMail);
};


exports.sendMailToReferredUser = function(name, email, referred_by_code){
    var to = email;
    var sub = "You have just earned a FREE ride on "+config.get('projectName')+"";
    var msg = "Hey " + name.split(' ')[0] +",\n\n" +
        "Great to see you on "+config.get('projectName')+". You have used the referral code \"" + referred_by_code + "\" to use "+config.get('projectName')+" and we are giving you a free ride for it. " +
        "We hope you will use "+config.get('projectName')+" soon and will find it incredibly useful.\n\n" ;
    msg += config.get('emailCredentials.signature');

    utils.sendEmailForPassword(to, msg, sub, function(result){
    });
};


exports.sendMailForFirstReferralRide = function (friends_name, referring_user){
    var first_name = friends_name.split(' ')[0];
    var to = referring_user.user_email;
    var sub = first_name + " has just taken the first ride on "+config.get('projectName')+" and you have been credited a FREE ride";
    var msg = "Hey " + referring_user.user_name.split(' ')[0] + ",\n\n" +
        "Congratulations. " + first_name + " has just taken the first ride with "+config.get('projectName')+". We have credited a free ride in your account. Have fun with "+config.get('projectName')+".\n\n" ;
    msg += config.get('emailCredentials.signature');

    //console.log("Sending message to the REFERRING USER for the first ride taken");
    utils.sendEmailForPassword(to, msg, sub, function(result){
    });
};

