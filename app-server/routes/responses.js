/**
 * Created by harsh on 12/3/14.
 */

var constants = require('./constants');
var logging = require('./logging');

exports.errorResponse = function (res) {
    var response = {"error": 'something went wrong', "flag": constants.responseFlags.ERROR_IN_EXECUTION};
    logging.logResponse(response);
    res.send(JSON.stringify(response));
};

exports.parameterMissingResponse = function (res) {
    var response = {"error": "some parameter missing", "flag": constants.responseFlags.PARAMETER_MISSING};
    logging.logResponse(response);
    res.send(JSON.stringify(response));
};

exports.authenticationErrorResponse = function (res){
    var response = {"error": 'invalid access token', "flag": constants.responseFlags.INVALID_ACCESS_TOKEN};
    logging.logResponse(response);
    res.send(JSON.stringify(response));
};

exports.sendError = function (res) {
    var response = {"error": 'something went wrong', "flag": constants.responseFlags.ERROR_IN_EXECUTION};
    logging.logResponse(response);
    res.send(JSON.stringify(response));
};