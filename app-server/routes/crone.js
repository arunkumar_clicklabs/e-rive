var request = require('request');
var aysnc = require('async');
var fs = require('fs');
var utils = require('./../utils/commonfunction');
var datetime_utils = require('./../utils/datetimefunctions');
var logging = require('./logging');
var constants = require('./constants');
var mailer = require('./mailer');
var messenger = require('./messenger');
var login = require('./user_login');


// Crone to define the undefined pickup locations
// The locations are undefined because of the previous logic of populating the field
// only when the ride has started
// We have made changes to the logic where the address gets populated now to get the
// when the customer sends a request and hence, is the logic for retreiving the calls
// missed by the driver becomes easy
exports.define_undefined_pickup_location_addresses = function(req, res)
{
    var intervalID = setInterval(callBatch, 2000);

    function callBatch()
    {
        var changed = define_undefined_pickup_location_addresses_for_batch(function(changed){
            if(!changed)
            {
                clearInterval(intervalID);
                res.send({"log": "All addresses has been defined."});
            }
        });
    }
}


function define_undefined_pickup_location_addresses_for_batch(callback)
{
    var get_addresses = "SELECT `engagement_id`, `pickup_latitude`, `pickup_longitude`, `pickup_location_address` " +
        "FROM `tb_engagements`";
    connection.query(get_addresses, function(err, result_engagements) {
        logging.logDatabaseQuery("Fetching addresses for engagements with status 4 and 6.", err, result_engagements, null);

        var num_engagements = result_engagements.length;

        // Get all the engagements for which the addresses are undefined
        var engagements = {};
        var requests_made = 0;
        var responses_rec = 0;
        var counter = 0;
        for (var i = 0; i < num_engagements && requests_made<10; i++) {
            var existing_address = result_engagements[i].pickup_location_address;
            if (!existing_address || existing_address==="Unnamed") {
                var engagement_id = result_engagements[i].engagement_id;
                var latitude = result_engagements[i].pickup_latitude;
                var longitude = result_engagements[i].pickup_longitude;
                fetchAndUpdatePickupAddress(engagement_id, latitude, longitude);
                requests_made++;
            }
        }
        callback(requests_made != 0);

        function fetchAndUpdatePickupAddress(engagement_id, latitude, longitude)
        {
            request('http://maps.googleapis.com/maps/api/geocode/json?latlng=' + latitude + ',' + longitude, function (error, response, body) {
                responses_rec++;
                if (!error && response.statusCode == 200) {
                    body = JSON.parse(body);
                    if (body.results.length > 0) {
                        var raw_address = body.results[0].formatted_address;
                        var pickup_address = utils.formatLocationAddress(raw_address);

                        var update_address = "UPDATE `tb_engagements` SET `pickup_location_address`=? WHERE `engagement_id`=?";
                        connection.query(update_address, [pickup_address, engagement_id], function (err, result) {
                            logging.logDatabaseQuery("Updating undefined pickup location address.", err, result, null);
                        });
                    }
                }
            });
        }
    });
};


//exports.make_verification_calls = function(req, res){
//    logging.startSection("making verification calls for unverified users");
//
//    var get_unverified_users =
//        "SELECT `user_id`, `phone_no`, `verification_calls` " +
//        "FROM `tb_users` WHERE `verification_status` = 0 && `verification_calls` = 0 " +
//            "&& `date_registered` > DATE_SUB(NOW(), INTERVAL 7 MINUTE) && `date_registered` < DATE_SUB(NOW(), INTERVAL 5 MINUTE)";
//    connection.query(get_unverified_users, [], function(err, users){
//        logging.logDatabaseQuery("Getting the unverified users registered within last 3 minutes", err, users);
//
//        var i=0;
//        //var update_num_calls = "UPDATE `tb_users` SET `verification_calls`=`verification_calls`+1 WHERE `user_id`=?";
//        for(i = 0; i < users.length; i++){
//            var req = {body: {phone_no: users[i].phone_no}};
//            var res_dummy = {
//                send: function(message){
//                    console.log(message);
//            }};
//            login.send_otp_via_call(req, res_dummy);
//
//            //connection.query(update_num_calls, [users[i].user_id], function(err, result){
//            //    logging.logDatabaseQuery("Incrementing the number of verification calls", err, result);
//            //});
//        }
//        var response = "Making verification calls to " + users.length + " users";
//        logging.logResponse(response);
//        res.send(response);
//    });
//};


function emptyFunction(){
    console.log("Running the empty function");
}


exports.removeInactiveEngagements = function(req, res) {

    var sql = "SELECT `engagement_id`,`current_time`,`driver_id` FROM `tb_engagements` WHERE `status` IN (0,1,2)";
    connection.query(sql, function(err, result) {
        if (!err) {
            var result_length = result.length;
            if (result_length > 0) {

                var engage = [];
                var technician = [];
                for (var i = 0; i < result_length; i++) {

                    var diff = datetime_utils.getTimeDifferenceInMinutes(result[i].current_time, 2);
                    if (diff >= 120)
                    {
                        engage.push(result[i].engagement_id);
                        technician.push(result[i].driver_id);
                    }

                }
                if (engage.length > 0)
                {
                    var engageStr = engage.toString(",");
                    var technicianStr = technician.toString(",");

                    var sql11 = "UPDATE `tb_users` SET `status`= ? WHERE user_id IN (" + technicianStr + ")";
                    connection.query(sql11, [0], function(err, result11) {

                    });
                    var sql11 = "UPDATE `tb_engagements` SET `status`= ? WHERE engagement_id IN (" + engageStr + ")";
                    connection.query(sql11, [7], function(err, result1) {

                    });

                    var response = {"log": 'crone successful'};
                    res.send(JSON.stringify(response));
                }
                else
                {
                    var response = {"log": 'crone successful'};
                    res.send(JSON.stringify(response));
                }

            }
            else
            {
                var response = {"log": 'crone successful'};
                res.send(JSON.stringify(response));
            }

        }
        else
        {
            var response = {"log": 'crone successful'};
            res.send(JSON.stringify(response));
        }
    });
};



function getOfflineDrivers(callback) {
    var get_offline_drivers =
        "SELECT `user_id`, `timestamp`, `user_name`, `phone_no`, `reg_as` " +
        "FROM `tb_users` " +
        "WHERE `current_user_status` = ? && `reg_as`=? && `status` = ? && `driver_suspended` = 0 && `timestamp` < (NOW() - INTERVAL 10 MINUTE)";
    var values = [constants.userCurrentStatus.DRIVER_ONLINE,
        constants.userRegistrationStatus.DRIVER,
        constants.userFreeStatus.FREE];
    connection.query(get_offline_drivers, values, function (err, inactive_drivers) {
        logging.logDatabaseQuery("Getting inactive drivers", err, inactive_drivers, null);
        callback(err, inactive_drivers);
    });
}


exports.update_lat_long_of_driver_to_zero = function(req, res){
    logging.startSection("update_lat_long_of_driver_to_zero");
    logging.logRequest(req);

    getOfflineDrivers(function(err, inactive_drivers){
        if (!err) {
            if (inactive_drivers.length > 0) {
                var inactive_drivers_length = inactive_drivers.length;
                var arr_users = [];
                for (var i = 0; i < inactive_drivers_length; i++) {
                    arr_users.push(inactive_drivers[i].user_id);
                }
                var str_users = arr_users.toString();
                var sql4 = "UPDATE `tb_users` SET `current_location_latitude`=?,`current_location_longitude`=? WHERE `user_id` IN (" + str_users + ")";
                connection.query(sql4, [0,0], function(err, result4) {
                    if (!err) {
                        var response = {"log": 'crone successful'};
                        res.send(JSON.stringify(response));
                    }
                });
            }
            else{
                var response = {"log": 'crone successful'};
                res.send(JSON.stringify(response));
            }
        }
    });
};
