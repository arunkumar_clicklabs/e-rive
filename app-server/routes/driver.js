
var async       = require('async');

var logging     = require('./logging');
var mailer      = require('./mailer');
var responses   = require('./responses');
var constants   = require('./constants');
var utils       = require('./../utils/commonfunction');


exports.get_all_drivers_status              = getAllDriversStatus;
exports.changeAvailability                  = changeAvailability;
exports.updateLocation                      = updateLocation;
exports.getDriverActivityLogs               = getDriverActivityLogs;


function changeAvailability (req, res){
    logging.startSection("change_availability");
    logging.logRequest(req);

    var accessToken     = req.body.access_token;
    var latitude        = req.body.latitude;
    var longitude       = req.body.longitude;
    var availability    = parseInt(req.body.flag);

    utils.authenticateUser(accessToken, function (user){
        if(user == 0){
            responses.authenticationErrorResponse(res);
            return;
        }

        var userId = user[0].user_id;

        var updateLocationAndFlag =
            "UPDATE `tb_users` " +
            "SET `current_location_latitude` = ?, `current_location_longitude` = ?, `is_available` = ? " +
            "WHERE `user_id` = ?";
        var values = [latitude, longitude, availability, userId];
        connection.query(updateLocationAndFlag, values, function(err, result){
            if(err){
                logging.logDatabaseQuery("Toggling the availability of the driver", err, result);
            }

            var response = {
                flag: constants.responseFlags.ACTION_COMPLETE,
                message: availability == 0 ? "Requests will NOT be sent to you now." : "Requests will be sent to you now."
            };
            res.send(response);
        });

        var logTimings = "INSERT INTO `tb_availability_logs` (`user_id`, `availability`) VALUES (?, ?)";
        connection.query(logTimings, [userId, availability], function(err, result){
            if(err){
                logging.logDatabaseQuery("Logging the timings for availability changes.", err, result);
            }
        });
    });
}



function updateLocation(req, res)
{
    logging.startSection("update_driver_location");
    logging.logRequest(req);

    var accessToken     = req.body.access_token;
    var latitude        = req.body.latitude;
    var longitude       = req.body.longitude;
    var deviceToken     = req.body.device_token;

    var response = {};

    var manvalues = [accessToken, latitude, longitude, deviceToken];
    var checkblank = utils.checkBlank(manvalues);
    if (checkblank == 1)
    {
        response = {"error": 'some parameter missing', "flag": 0};
        //logging.logResponse(response);
        res.send(response);
        return;
    }

    utils.authenticateUser(accessToken, function(user) {
        if (user == 0)
        {
            response = {"error": 'Invalid access token', "flag": 1};
            //logging.logResponse(response);
            res.send(response);
            return;
        }

        var userId = user[0].user_id;
        console.log('USER ID is ' + userId);
        console.log('User: ' + JSON.stringify(user[0], undefined, 2));


        if(user[0].current_user_status === constants.userCurrentStatus.DRIVER_ONLINE) {
            var updateCurrentLocation =
                "UPDATE `tb_users` " +
                "SET `current_location_latitude` = ?, `current_location_longitude` = ?, `user_device_token` = ? " +
                "WHERE `user_id`=?";
            connection.query(updateCurrentLocation, [latitude, longitude, deviceToken, userId], function(err, result) {
                logging.logDatabaseQuery('Updating the current location of the user', err, result);
            });

            var logLocationUpdate =
                "INSERT INTO `tb_updated_user_location` " +
                "(`user_id`, `latitude`, `longitude`) " +
                "VALUES(?, ?, ?)";
            connection.query(logLocationUpdate, [userId, latitude, longitude],function(err, result){
                logging.logDatabaseQuery('Inserting the logs for the driver', err, result);
            });
        }

        response = {"log": "Updated"};
        logging.logResponse(response);
        res.send(response);
    });
};



function getAllDriversStatus(req, res){
    logging.startSection("Getting the status for all the drivers");

    var fetchStatus =
        "SELECT * FROM " +
            "(SELECT `user_id`, `user_name`, `phone_no`, `is_available`, `status`, `current_user_status`, `reg_as`, `current_location_latitude`, `current_location_longitude` " +
            "FROM `tb_users` WHERE `reg_as` IN (1,2)) as `drivers` " +
        "JOIN " +
            "tb_timings " +
        "ON drivers.user_id = tb_timings.driver_id";
    connection.query(fetchStatus, [], function(err, drivers){
        if(err){
            logging.logDatabaseQueryError("Getting the status of all drivers through monitoring", err, drivers);
        }

        var i = 0;
        for(i = 0; i < drivers.length; i++){
            drivers[i].reg_as       = utils.registrationStatusToString(drivers[i].reg_as);
            drivers[i].status       = utils.driverStatusToString(drivers[i].status);
            drivers[i].is_available = utils.jugnooStatusToString(drivers[i].is_available);
        }

        utils.sortByKeyAsc(drivers, 'reg_as');

        res.send(drivers);
    });
}


function getDriverActivityLogs(req, res){
    logging.startSection('Getting the recent activity logs for the driver');
    logging.logRequest(req);

    var driverId    = req.body.driver_id;
    var password    = req.body.password;

    if(password !== constants.SUPER_ADMIN_PASSWORD){
        res.send('You are not authorized to perform this function');
        return;
    }

    var driver = {};
    async.parallel(
        [
            getDriverInformation.bind(null, driverId, driver),
            getDriverTimings.bind(null, driverId, driver),
            getAvailabilityLogs.bind(null, driverId, driver),
            getLocationUpdateLogs.bind(null, driverId, driver),
            getRecentEngagements.bind(null, driverId, driver)
        ],
        function(err){
            if(err){
                process.stderr.write('Some error occured when getting the logs for the driver\n');
            }

            driver.registered_as = utils.registrationStatusToString(driver.registered_as);
            driver.status        = utils.driverStatusToString(driver.status);
            driver.availability  = utils.jugnooStatusToString(driver.availability);

            var i = 0;
            for(i = 0; i < driver.availability_logs.length; i++){
                driver.availability_logs[i].availability = utils.jugnooStatusToString(driver.availability_logs[i].availability);
            }

            res.send(JSON.stringify(driver, undefined, 2));
        }
    );

    function getDriverInformation(driverId, driver, callback){
        var getDriverInformation =
            'SELECT `user_id`, `user_name`, `phone_no`, `status`, `current_user_status`, `is_available`, `reg_as`, `current_location_latitude`, `current_location_longitude`, `driver_suspended` ' +
            'FROM `tb_users` ' +
            'WHERE `user_id` = ?';
        connection.query(getDriverInformation, [driverId], function(err, user){
            logging.logDatabaseQuery('Getting the basic information for the driver', err, user);

            if(err)
                return process.nextTick(callback.bind(null, err));

            user = user[0];

            driver.id        = user.user_id;
            driver.name      = user.user_name;
            driver.phone_no  = user.phone_no;
            driver.status    = user.status;
            driver.latitude  = user.current_location_latitude;
            driver.longitude = user.current_location_longitude;
            driver.suspended = user.driver_suspended;
            driver.availability  = user.is_available;
            driver.registered_as = user.reg_as;

            return process.nextTick(callback.bind(null, err));
        });
    }

    function getDriverTimings(driverId, driver, callback){
        var getTimings =
            'SELECT `driver_id`, ADDTIME(`start_time`, \'5:30:00\') AS `start_time`, ADDTIME(`end_time`, \'5:30:00\') AS `end_time` ' +
            'FROM `tb_timings` ' +
            'WHERE `driver_id` = ? ' +
            'ORDER BY `start_time`';
        connection.query(getTimings, [driverId], function(err, timings){
            logging.logDatabaseQuery('Getting the duty timings for the driver', err, timings);

            if(err)
                return process.nextTick(callback.bind(null, err));

            driver.timings = [];
            var i = 0;
            for(i = 0; i < timings.length; i++){
                driver.timings.push({
                    start : timings[i].start_time,
                    end   : timings[i].end_time
                });
            }

            return process.nextTick(callback.bind(null, err));
        });
    }

    function getAvailabilityLogs(driverId, driver, callback){
        var getLogs =
            'SELECT `availability`, TIMESTAMPADD(MINUTE, 330, `logged_on`) as `changed_on` ' +
            'FROM `tb_availability_logs` ' +
            'WHERE `user_id` = ? ' +
            'ORDER BY `logged_on` DESC ' +
            'LIMIT 4';
        connection.query(getLogs, [driverId], function(err, logs){
            logging.logDatabaseQuery('Getting the availability logs for the driver', err, logs);

            if(err)
                return process.nextTick(callback.bind(null, err));

            driver.availability_logs = logs;

            return process.nextTick(callback.bind(null, err));
        });
    }

    function getLocationUpdateLogs(driverId, driver, callback){
        var getLogs =
            'SELECT TIMESTAMPADD(MINUTE, 330, `location_updated_timestamp`) as `updated_on` ' +
            'FROM `tb_updated_user_location` ' +
            'WHERE `user_id` = ? ' +
            'ORDER BY `location_updated_timestamp` DESC ' +
            'LIMIT 10';
        connection.query(getLogs, [driverId], function(err, logs){
            logging.logDatabaseQuery('Getting the logs for location updates for the driver', err, logs);

            if(err)
                return process.nextTick(callback.bind(null, err));

            var i = 0;
            driver.location_updates = [];
            for(i = 0; i < logs.length; i++){
                driver.location_updates.push(logs[i].updated_on);
            }

            return process.nextTick(callback.bind(null, err));
        });
    }

    function getRecentEngagements(driverId, driver, callback){
        var getEngagements =
            'SELECT TIMESTAMPADD(MINUTE, 330, `current_time`) as requested_on, status, accept_distance, TIMESTAMPDIFF(SECOND, request_made_on, request_received_on) AS reached_in ' +
            'FROM `tb_engagements` ' +
            'WHERE `driver_id` = ? AND `status` IN (1, 2, 3, 4, 6, 8) ' +
            'ORDER BY `current_time` DESC LIMIT 10';
        connection.query(getEngagements, [driverId], function(err, engagements){
            logging.logDatabaseQuery('Getting the last 10 engagements for the driver', err, engagements);

            if(err)
                return process.nextTick(callback.bind(null, err));

            driver.engagements = engagements;
            return process.nextTick(callback.bind(null, err));
        });
    }
}