var utils = require('./../utils/commonfunction');
var logging = require('./logging');
var constants = require('./constants');
var responses = require('./responses');


exports.test_sending_sms = function(req, res){
    var contact_number = req.body.contact_number;

    var manvalues = [contact_number];
    var checkblank = utils.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
    }
    else {
        var accountSid = config.get('twillioCredentials.accountSid');
        var authToken = config.get('twillioCredentials.authToken');

        var client = require('twilio')(accountSid, authToken);
        client.messages.create({
            to: contact_number, // Any number Twilio can deliver to
            from: config.get('twillioCredentials.fromNumber'),
            body: 'Testing SMS service'// body of the SMS message
        }, function(err, message) {
            console.log("Testing sms service: " + "Error: " + err );
            console.log("Testing sms service: " + "Message: " + message );
            res.send({"error":JSON.stringify(err), "message":JSON.stringify(message)});
        });
    }
};


exports.test_email_service = function(req,res){
    var receiver_id = req.body.receiver_id;

    var manvalues = [receiver_id];
    var checkblank = utils.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
    }
    else {
        var to = receiver_id;
        var sub = "test email";
        var msg = '<a href="www.google.com">test email</a>';

        utils.sendEmailForPassword(to, msg, sub, function(resultEmail)
        {
            console.log("The result for the sending email is: " + resultEmail);
        });
    }
};


exports.check_push_notification = function(req, res){
    var user_id = req.body.user_id;
    var num_notifications = req.body.num;

    var message = "This is a test notification";
    var flag = 1;       // silent notification for iOS
    var payload = {"log": "no drivers available", "flag": constants.notificationFlags.NO_DRIVERS_AVAILABLE};

    var get_user_device_info = "SELECT `user_id`,`device_type`,`user_device_token` FROM `tb_users` WHERE `user_id`=?";
    connection.query(get_user_device_info, [user_id], function (err, result_user)
    {
        logging.logDatabaseQuery("Get device information for the driver", err, result_user, null);

        if (result_user[0].device_type == constants.deviceType.ANDROID)
        {
            for(var i=0; i < num_notifications; i++)
                utils.sendAndroidPushNotification(result_user[0].user_device_token, payload);
        }
        else if (result_user[0].device_type == constants.deviceType.iOS) {
            for (var i = 0; i < num_notifications; i++)
                utils.sendIosPushNotification(result_user[0].user_device_token, message, flag, payload);
        }
    });

    res.send(JSON.stringify({log: "sending " + num_notifications + " notifications to " + user_id}));
};