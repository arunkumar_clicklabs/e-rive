
var constants   = require('./constants');
var logging     = require('./logging');
var responses   = require('./responses');
var utils       = require('./../utils/commonfunction');



exports.rateTheDriver        = addRatingForDriver;
exports.skipRatingByCustomer = skipRatingByCustomer;



function addRatingForDriver(req, res){
    logging.startSection('Adding rating for the driver');
    logging.logRequest(req);

    var accessToken  = req.body.access_token;
    var givenRating  = req.body.given_rating;
    var engagementId = req.body.engagement_id;
    var driverId     = req.body.driver_id;
    var feedback     = req.body.feedback;

    utils.authenticateUser(accessToken, function(user){
        if(user == 0){
            responses.authenticationErrorResponse(res);
            return;
        }

        var updateEngagement =
            'UPDATE `tb_engagements` ' +
            'SET `driver_rating` = ?, `driver_feedback` = ? ' +
            'WHERE `engagement_id` = ?';
        connection.query(updateEngagement, [givenRating, feedback, engagementId], function(err, result){
            logging.logDatabaseQuery('Updating the rating and feedback by customer for the engagement', err, result);
        });

        var updateDriverRating =
            'UPDATE `tb_users` ' +
            'SET `total_rating_got_driver` = `total_rating_got_driver` + 1, `total_rating_driver` = `total_rating_driver` + ? ' +
            'WHERE `user_id` = ?';
        connection.query(updateDriverRating, [givenRating, driverId], function(err, result){
            logging.logDatabaseQuery('Updating the total rating for the driver', err, result);
        });

        var response = {
            flag : constants.responseFlags.ACTION_COMPLETE,
            log  : 'Rating added.'};
        res.send(response);
    });
}




function skipRatingByCustomer(req, res){
    logging.startSection('Skip the rating screen for the customer');
    logging.logRequest(req);

    var accessToken  = req.body.access_token;
    var engagementId = req.body.engagement_id;

    utils.authenticateUser(accessToken, function(user) {
        if (user == 0) {
            responses.authenticationErrorResponse(res);
            return;
        }

        var updateEngagement =
            'UPDATE `tb_engagements` ' +
            'SET `skip_rating_by_customer` = 1 ' +
            'WHERE `engagement_id` = ?';
        connection.query(updateEngagement, [engagementId], function (err, result) {
            logging.logDatabaseQuery('Skipping the rating by the customer for the engagement', err, result);
        });

        var response = {
            flag : constants.responseFlags.ACTION_COMPLETE,
            log  : 'Rating skipped.'};
        res.send(response);
    });
}




