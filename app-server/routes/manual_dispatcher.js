/**
 * Created by harsh on 11/3/14.
 */

var async = require('async');
var fs = require('fs');

var constants = require("./constants");
var dispatcher = require("./ride_parallel_dispatcher");
var utils = require("./../utils/commonfunction");
var datetime_utils = require('./../utils/datetimefunctions');
var logging = require('./logging');
var mailer = require('./mailer');
var messenger = require('./messenger');
var responses = require('./responses');

function sendRequest(access_token, pickup_id, latitude, longitude, car_type){
    // Create a request
    var req = {
        body: {
            access_token: access_token,
            latitude: latitude,
            longitude: longitude,
            duplicate_flag: 0,
            pickup_id: pickup_id,
            car_type : car_type
        }
    };
    // Create a response with a method send
    var res = {
        send: function(response){
            console.log(response);
        }
    };

    dispatcher.request_ride(req, res);
}

// Use this map to see if the user has been notified earlier or not
// Used to avoid sending the driver information to the user again and again
var customer_notified = {};

exports.schedule_request_if_any = function(req, res){
    logging.startSection("schedule_request_if_any");
    logging.logRequest(req);

    var get_schedules = "SELECT `pickup_id`, `user_id`, `latitude`, `longitude`, `address`, `pickup_time`, `session_id`, `num_tries`,`car_type` " +
        "FROM `tb_schedules` " +
        "WHERE `pickup_time` > NOW() && `pickup_time` <  timestamp(DATE_ADD(NOW(), INTERVAL ? MINUTE))";
    connection.query(get_schedules, [constants.SCHEDULER_ALARM_TIME], function(err, pickups){
        logging.logDatabaseQuery("Getting the schedules for the ride", err, pickups, null);
        if(pickups.length > 0){
            // TODO reduce the number of queries being made from here if the number of schedules increase
            for(var i=0; i<pickups.length; i++){

                function changeNumTries(pickup_id, new_value) {
                    var update_schedule = "UPDATE `tb_schedules` SET `num_tries` = ? WHERE `pickup_id`=?";
                    connection.query(update_schedule, [new_value, pickup_id], function (err, result) {
                        logging.logDatabaseQuery("Updating the number of tries for pickup: " + pickup_id, err, result, null);
                    });
                }

                function processManualPickup(pickup){

                    function getPreviousSessionAndEngagementStatus(user_id, callback) {
                        var get_session_and_engagements = "SELECT * " +
                            "FROM " +
                            "(SELECT `session_id`, `is_active`, `ride_acceptance_flag` FROM `tb_session` WHERE `user_id`=? ORDER BY `session_id` DESC LIMIT 1) AS session " +
                            "JOIN " +
                            "(SELECT `engagement_id`, `status`, `driver_id`, `session_id` FROM `tb_engagements` WHERE `user_id`=?) as engagements "  +
                            "WHERE session.session_id = engagements.session_id";
                        connection.query(get_session_and_engagements, [user_id, user_id], function(err, engagements){
                            logging.logDatabaseQuery("Getting all engagements for the previous session for the customer", err, engagements, null);

                            var session_status = constants.sessionStatus.INACTIVE;
                            var engagement = {};
                            engagement.status = constants.engagementStatus.TIMEOUT;

                            if(engagements.length > 0){
                                var session_status = engagements[0].is_active;
                                for(var i=0; i<engagements.length; i++){
                                    if(engagements[i].status == constants.engagementStatus.ACCEPTED){
                                        engagement.status = constants.engagementStatus.ACCEPTED;
                                        engagement.driver_id = engagements[i].driver_id;
                                        break;
                                    }
                                    if(engagements[i].status == constants.engagementStatus.STARTED){
                                        engagement.status = constants.engagementStatus.STARTED;
                                        engagement.driver_id = engagements[i].driver_id;
                                        break;
                                    }
                                    if(engagements[i].status == constants.engagementStatus.ENDED){
                                        engagement.status = constants.engagementStatus.ENDED;
                                        engagement.driver_id = engagements[i].driver_id;
                                        break;
                                    }
                                    if(engagements[i].status == constants.engagementStatus.ACCEPTED_THEN_REJECTED){
                                        engagement.status = constants.engagementStatus.ACCEPTED_THEN_REJECTED;
                                        engagement.driver_id = engagements[i].driver_id;
                                        break;
                                    }
                                }
                            }

                            console.log("callback using session_status: " + session_status + " engagement_status: " + utils.engagementStatusToString(engagement.status));
                            callback(session_status, engagement);
                        });
                    }

                    getPreviousSessionAndEngagementStatus(pickup.user_id, function (session_status, engagement) {
                        //console.log("pickup: " + JSON.stringify(pickup, undefined, 4));

                        // See if the user has already initiated a ride request before making a new request
                        // We know that the user is using the app in this case and hence,
                        // we won't be scheduling rides manually for the user
                        if(pickup.num_tries == 0) {
                            // The user has already initiated a request using the app, so let's not process it anymore
                            if (session_status == constants.sessionStatus.ACTIVE){
                                // update the number of tries to max + 1 so that this schedule is not processed again
                                pickup.num_tries = constants.MANUAL_NUM_TRIES + 1;
                                changeNumTries(pickup.pickup_id, pickup.num_tries);
                                //console.log("PICKUP TRY #1: THE USER ALREADY HAS AN ACTIVE SESSION");
                            }
                        }

                        if(pickup.num_tries > 0 && pickup.num_tries <= constants.MANUAL_NUM_TRIES){
                            // If this is not the first pickup try, see if the request has been accepted
                            // in which case send the customer a message using the driver_id from the active engagement
                            if(pickup.num_tries != 0){
                                if(session_status == constants.sessionStatus.ACTIVE &&
                                    (engagement.status == constants.engagementStatus.ACCEPTED || engagement.status == constants.engagementStatus.STARTED)){
                                    console.log("notification sent earlier: " + customer_notified[pickup.pickup_id.toString()]);
                                    if(!customer_notified.hasOwnProperty(pickup.pickup_id.toString())
                                        || customer_notified[pickup.pickup_id.toString()] === false){
                                        messenger.sendDriverInformation(pickup.user_id, engagement.driver_id, pickup.pickup_time);
                                        customer_notified[pickup.pickup_id.toString()] = true;
                                        console.log("SENDING THE MESSAGE TO THE CUSTOMER");
                                    }
                                }
                            }
                        }

                        if(pickup.num_tries < constants.MANUAL_NUM_TRIES) {
                            // If the previous session for the customer is not inactive or has been timed out, initiate a new request
                            if(session_status == constants.sessionStatus.INACTIVE || session_status == constants.sessionStatus.TIMED_OUT){
                                var customer_info = "SELECT `access_token`, `phone_no`, `user_name`, `user_id`, `user_email` " +
                                    "FROM `tb_users` WHERE `user_id`=?";
                                connection.query(customer_info, [pickup.user_id], function(err, customer) {
                                    logging.logDatabaseQuery("Access token for the customer", err, customer, null);
                                    // Initiate the request process
                                    console.log("SENDING THE REQUEST TO CREATE A NEW SESSION");
                                    customer_notified[pickup.pickup_id] = false;
                                    sendRequest(customer[0].access_token, pickup.pickup_id, pickup.latitude, pickup.longitude, pickup.car_type);
                                });
                            }
                            // Update the table of schedules to increase the number of tries
                            pickup.num_tries += 1;
                            changeNumTries(pickup.pickup_id, pickup.num_tries);
                        }
                        else if(pickup.num_tries == constants.MANUAL_NUM_TRIES) {
                            console.log("REACHED THE MAXIMUM NUMBER OF TRIES FOR THE PICKUP");
                            // Send a message if we couldn't provide a ride to the customer
                            if((session_status == constants.sessionStatus.INACTIVE && engagement.status == constants.engagementStatus.ACCEPTED_THEN_REJECTED)
                                || (session_status == constants.sessionStatus.INACTIVE && engagement.status == constants.engagementStatus.TIMEOUT)
                                || session_status == constants.sessionStatus.TIMED_OUT){

                                var sql = "UPDATE `tb_schedules` SET `failed_to_pickup` = ? WHERE `pickup_id`=? LIMIT 1"
                                connection.query(sql,[1, pickup.pickup_id], function(err,result){

                                });
                                // send message to the customer
                                console.log("SENDING MESSAGE OF FAILURE TO THE USER");
                                messenger.sendMessageForManualFailure(pickup.user_id, pickup.pickup_time);
                                // Update the table of schedules to increase the number of tries
                                pickup.num_tries += 1;
                                changeNumTries(pickup.pickup_id, pickup.num_tries);
                            }
                        }
                    });
                }

                processManualPickup(pickups[i]);
            }
            var response = {};
            response.log = "Scheduled " + pickups.length + " pickups";
            res.send(JSON.stringify(response));
        }
        else{
            res.send(JSON.stringify({log:"No pickups to schedule right now"}));
        }
    });
};


exports.insert_pickup_schedule = function(req, res){
    logging.startSection("insert_pickup_schedule");
    logging.logRequest(req);

    var accessToken = req.body.access_token;
    var userId = req.body.user_id;
    var latitude = req.body.latitude;
    var longitude = req.body.longitude;
    var pickupTime = req.body.pickup_time;
    var carType = req.body.car_type;

    var manualDestinationLatitude = req.body.manual_destination_latitude;
    var manualDestinationLongitude = req.body.manual_destination_longitude;
    var manualDestinationAddress = req.body.manual_destination_address;

    if(typeof manualDestinationLatitude === 'undefined')
        manualDestinationLatitude = 0;
    if(typeof manualDestinationLongitude === 'undefined')
        manualDestinationLongitude = 0;
    if(typeof manualDestinationAddress === 'undefined')
        manualDestinationAddress = '';
    if(typeof carType === 'undefined') {
        carType = 0;
    }

    if(typeof accessToken == 'undefined'){
        insertPickupScheduleThroughSupport(userId, latitude, longitude, pickupTime, manualDestinationLatitude, manualDestinationLongitude, manualDestinationAddress, carType, res);
    }
    else{
        insertPickupScheduleThroughApp(accessToken, latitude, longitude, pickupTime, manualDestinationLatitude, manualDestinationLongitude, manualDestinationAddress, carType, res);
    }
};

function isAValidScheduleTime(date, currentTimeDiff, daysLimit){
    console.log("date is " + date);
    var utcTime = datetime_utils.changeTimezoneFromIstToUtc(date);
    console.log("utc time is " + utcTime);
    var timeNow = new Date();
    console.log("current time is " + timeNow.toISOString());

    // If the time difference is less than an hour for the schedule, then show user a message
    console.log("difference is minutes " + datetime_utils.timeDifferenceInMinutes(timeNow, utcTime));
    console.log("difference is days " + datetime_utils.timeDifferenceInDays(timeNow, utcTime));
    var isValid = datetime_utils.timeDifferenceInMinutes(timeNow, utcTime) > currentTimeDiff && datetime_utils.timeDifferenceInDays(timeNow, utcTime) <= daysLimit;
    console.log("valid? " + isValid);
    return isValid;
}

function insertSchedule(userId, latitude, longitude, pickupTime, manualDestinationLatitude, manualDestinationLongitude, manualDestinationAddress, carType, sendMessage, res){
    pickupTime = datetime_utils.changeTimezoneFromIstToUtc(pickupTime);
    utils.getLocationAddress(latitude, longitude, function(address){
        var insert_schedule = "INSERT INTO `tb_schedules` (`user_id`, `latitude`, `longitude`, `address`, `pickup_time`,`manual_destination_latitude`,`manual_destination_longitude`,`manual_destination_address`, `car_type`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
        var values = [userId, latitude, longitude, address, pickupTime, manualDestinationLatitude, manualDestinationLongitude, manualDestinationAddress, carType];
        connection.query(insert_schedule, values, function(err, result){
            logging.logDatabaseQuery("Added a new pickup schedule", err, result, null);

            var response = {
                flag: constants.responseFlags.SHOW_MESSAGE,
                message: "A "+config.get('projectName')+" driver will come and pick you up from " + address + " at your requested time."
            };
            res.header("Access-Control-Allow-Origin", "*");
            res.send(JSON.stringify(response));

            var pickup = {latitude: latitude, longitude: longitude, address: address, time: pickupTime, car_type : carType};
            mailer.sendScheduleConfirmationMail(userId, pickup);
            if(sendMessage === true){
                messenger.sendMessageForScheduledRide(userId, pickupTime, address);
            }
        });
    });
}

function insertPickupScheduleThroughSupport(userId, latitude, longitude, pickupTime, manualDestinationLatitude, manualDestinationLongitude, manualDestinationAddress, carType, res){
    var ensureValues = [userId, latitude, longitude, pickupTime];
    var isBlank = utils.checkBlank(ensureValues);
    if (isBlank === 1){
        responses.parameterMissingResponse(res);
        return;
    }

    // dummy values for the support system right now for validating the pickup time
    if(isAValidScheduleTime(pickupTime, 0, 7) === false){
        var response = {
            flag: constants.responseFlags.SHOW_ERROR_MESSAGE,
            error: "A pickup can only be scheduled after " + constants.SCHEDULE_CURRENT_TIME_DIFF + " minutes from now till midnight the next day."
        };
        res.send(response);
        return;
    }

    // hack to remove the unblocking of user when Animesh is entering schedule
    // Users are automatically blocked on more than 3 registrations from the same device
    var unblockUser = "UPDATE `tb_users` SET `is_blocked` = 0 WHERE user_id = ?";
    connection.query(unblockUser,[userId], function(err, result){
    });

    insertSchedule(userId, latitude, longitude, pickupTime, manualDestinationLatitude, manualDestinationLongitude, manualDestinationAddress, carType, true /* send message */, res);
}

function insertPickupScheduleThroughApp(accessToken, latitude, longitude, pickupTime, manualDestinationLatitude, manualDestinationLongitude, manualDestinationAddress, carType, res){
    var ensureValues = [accessToken, latitude, longitude, pickupTime];
    var isBlank = utils.checkBlank(ensureValues);
    if (isBlank === 1){
        responses.parameterMissingResponse(res);
        return;
    }

    utils.authenticateUser(accessToken, function(user) {
        if(user == 0){
            responses.authenticationErrorResponse(res);
            return;
        }

        if(isAValidScheduleTime(pickupTime, constants.SCHEDULE_CURRENT_TIME_DIFF, constants.SCHEDULE_DAYS_LIMIT) === false){
            var response = {
                flag: constants.responseFlags.SHOW_ERROR_MESSAGE,
                error: "A pickup can only be scheduled after " + constants.SCHEDULE_CURRENT_TIME_DIFF + " minutes from now till midnight the next day."
            };
            res.send(JSON.stringify(response));
            return;
        }

        insertSchedule(user[0].user_id, latitude, longitude, pickupTime, manualDestinationLatitude, manualDestinationLongitude, manualDestinationAddress, carType, false /* no message */, res);
    });
}


exports.show_pickup_schedules = function(req, res){
    logging.startSection("show_pickup_schedules");
    logging.logRequest(req);

    var accessToken = req.body.access_token;

    var values = [accessToken];
    var isBlank = utils.checkBlank(values);
    if(isBlank === 1){
        responses.parameterMissingResponse(res);
        return;
    }

    utils.authenticateUser(accessToken, function(user){
        if(user == 0){
            responses.authenticationErrorResponse(res);
            return;
        }

        var userId = user[0].user_id;
        var schedules = [];
        var termsAndConds = "";
        async.parallel(
            [
                getSchedules,
                getTermsAndConds
            ],
            function(err){
                if(err){
                    responses.errorResponse(res);
                    return;
                }

                var response = {
                    flag: constants.responseFlags.SCHEDULED_PICKUPS,
                    schedules: schedules,
                    terms: termsAndConds
                };
                logging.logResponse(response);
                res.send(JSON.stringify(response));
            }
        );

        function getSchedules(callback){
            //var get_schedules = "SELECT * FROM `tb_schedules` WHERE `user_id`=? AND `pickup_time` > NOW() ORDER BY `pickup_time` DESC";
            var get_schedules = "SELECT * FROM `tb_schedules` WHERE `user_id`=? ORDER BY `pickup_time` DESC";
            connection.query(get_schedules, [userId], function(err, results) {
                logging.logDatabaseQuery("Getting the schedules for the customers", err, results);
                if(err){
                    callback(err);
                }

                var timeDiff = 0;
                var status = "";
                var modifiable = 0;
                var i = 0;
                for (i = 0; i < results.length; i++) {
                    timeDiff = datetime_utils.timeDifferenceInMinutes(new Date(), results[i].pickup_time);
                    if (timeDiff > constants.SCHEDULE_CANCEL_WINDOW) {
                        status = constants.scheduleStatus.IN_QUEUE;
                        modifiable = 1;
                    }
                    else if (timeDiff > 0 && timeDiff <= 60) {
                        status = constants.scheduleStatus.IN_PROCESS;
                        modifiable = 0;
                    }
                    else if(results[i].failed_to_pickup == 1){
                        status = constants.scheduleStatus.COULD_NOT_PROCESS;
                        modifiable = 0;
                    }
                    else{
                        status = constants.scheduleStatus.PROCESSED;
                        modifiable = 0;
                    }

                    var time = datetime_utils.changeTimezoneFromUtcToIst(results[i].pickup_time);
                    time = time.replace(/T/, ' ').replace(/\..+/, '');

                    schedules.push({
                        pickup_id: results[i].pickup_id,
                        latitude: results[i].latitude,
                        longitude: results[i].longitude,
                        address: results[i].address,
                        time: time,
                        status: status,
                        modifiable: modifiable,
                        manual_destination_latitude : results[i].manual_destination_latitude,
                        manual_destination_longitude : results[i].manual_destination_longitude,
                        manual_destination_address : results[i].manual_destination_address
                    });
                }

                callback();
            });
        }

        function getTermsAndConds(callback){
            var fileName = __dirname + config.get('faqsHtmlFiles.tncSchedules');
            console.log("Reading terms and conditions from " + fileName);
            fs.readFile(fileName, {encoding: 'utf8'}, function (err, data){
                if(err){
                    console.log("Error: " + JSON.stringify(err) + " when reading the file " + fileName);
                    callback(err);
                    return;
                }

                termsAndConds = data.slice(0);
                console.log("Terms and conditions: " + termsAndConds);
                callback();
                return;
            });
        }

    });
};


exports.modify_pickup_schedule = function(req, res){
    logging.startSection("show_pickup_schedules");
    logging.logRequest(req);

    var accessToken = req.body.access_token;
    var pickupId = req.body.pickup_id;
    var latitude = req.body.latitude;
    var longitude = req.body.longitude;
    var pickupTime = req.body.pickup_time;

    var manualDestinationLatitude = req.body.manual_destination_latitude;
    var manualDestinationLongitude = req.body.manual_destination_longitude;
    var manualDestinationAddress = req.body.manual_destination_address;

    var values = [accessToken, pickupId, latitude, longitude, pickupTime];
    var isBlank = utils.checkBlank(values);
    if(isBlank === 1){
        responses.parameterMissingResponse(res);
        return;
    }

    if(typeof manualDestinationLatitude === 'undefined')
        manualDestinationLatitude = 0;
    if(typeof manualDestinationLongitude === 'undefined')
        manualDestinationLongitude = 0;
    if(typeof manualDestinationAddress === 'undefined')
        manualDestinationAddress = '';

    utils.authenticateUser(accessToken, function(user) {
        if (user == 0) {
            responses.authenticationErrorResponse(res);
            return;
        }

        var userId = user[0].user_id;
        utils.getLocationAddress(latitude, longitude, function(address){
            if(isAValidScheduleTime(pickupTime, constants.SCHEDULE_CURRENT_TIME_DIFF, constants.SCHEDULE_DAYS_LIMIT) === false){
                var response = {
                    flag: constants.responseFlags.SHOW_ERROR_MESSAGE,
                    error: "A pickup can only be scheduled after " + constants.SCHEDULE_CURRENT_TIME_DIFF + " minutes from now till midnight the next day."
                };
                res.send(JSON.stringify(response));
                return;
            }

            var modifySchedule = "UPDATE `tb_schedules` SET `latitude`=?, `longitude`=?, `address`=?, `pickup_time`=?,`manual_destination_latitude`=?,`manual_destination_longitude`=?,`manual_destination_address`=? WHERE pickup_id = ?";
            var values = [latitude, longitude, address, datetime_utils.changeTimezoneFromIstToUtc(pickupTime), manualDestinationLatitude, manualDestinationLongitude, manualDestinationAddress, pickupId];
            connection.query(modifySchedule, values, function(err, result){
                logging.logDatabaseQuery("Modifying an existing pickup schedule", err, result, null);

                var response = {
                    flag: constants.responseFlags.SHOW_MESSAGE,
                    message: "The scheduled pickup has been modified successfully. A "+config.get('projectName')+" driver will come and pick you up from " + address + " at your requested time."
                };
                res.send(JSON.stringify(response));

                var pickup = {
                    latitude: latitude,
                    longitude: longitude,
                    address: address,
                    time: pickupTime};
                mailer.sendScheduleConfirmationMail(userId, pickup);
            });
        });
    });
};


exports.remove_pickup_schedule = function(req, res){
    logging.startSection("remove_pickup_schedule");
    logging.logRequest(req);

    var accessToken = req.body.access_token;
    var pickupId = req.body.pickup_id;

    var values = [accessToken, pickupId];
    var isBlank = utils.checkBlank(values);
    if(isBlank === 1){
        responses.parameterMissingResponse(res);
        return;
    }

    utils.authenticateUser(accessToken, function(user) {
        if (user == 0) {
            responses.authenticationErrorResponse(res);
            return;
        }

        var getPickupData = "SELECT * FROM `tb_schedules` WHERE `pickup_id`=?";
        connection.query(getPickupData, [pickupId], function(err, pickups){
            if(err){
                logging.logDatabaseQuery("Getting the data for the pickup schedule", err, pickups);
                responses.errorResponse(res);
                return;
            }

            var pickupTime = pickups[0].pickup_time;
            if(canBeRemovedNow(pickupTime) === false){
                var response = {
                    flag: constants.responseFlags.SHOW_MESSAGE,
                    message: "Sorry. The pickup is already being processed and can't be removed now."
                };
                res.json(response);
                return;
            }

            var removeSchedule = "DELETE FROM `tb_schedules` WHERE pickup_id = ?";
            var values = [pickupId];
            connection.query(removeSchedule, values, function(err, result){
                logging.logDatabaseQuery("Deleting an existing pickup schedule", err, result, null);

                var response = {
                    flag: constants.responseFlags.SHOW_MESSAGE,
                    message: "The scheduled pickup has been removed successfully."
                };
                res.json(response);
            });
        });
    });
};

function canBeRemovedNow(pickupTime){
    var timeNow = new Date();
    if (datetime_utils.timeDifferenceInMinutes(timeNow, pickupTime) < constants.SCHEDULE_CANCEL_LIMIT){
        return false;
    }
    return true;
}
