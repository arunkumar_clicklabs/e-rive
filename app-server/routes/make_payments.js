var utils = require('./../utils/commonfunction');
var pushNotification    = require('./../utils/pushnotification');
var logging = require('./logging');
var braintree = require('braintree');
var constants = require('./constants');
var responses = require('./responses');
var ride_module = require('./ride_parallel_dispatcher');

var gateway = braintree.connect({
    environment: braintree.Environment.Sandbox,
    merchantId: config.get('braintreeCredentials.merchantId'),
    publicKey: config.get('braintreeCredentials.publicKey'),
    privateKey: config.get('braintreeCredentials.privateKey')
});


exports.createCustomer = createCustomer;
exports.addCreditCard = addCreditCard;
exports.listCreditCards = listCreditCards;
exports.ChangeDefaultCreditCard = ChangeDefaultCreditCard;
exports.DeleteCreditCard = DeleteCreditCard;
exports.paymentOnRideCompletion = paymentOnRideCompletion;
exports.getBraintreeToken = getBraintreeToken;
exports.checkPaymentStatus = checkPaymentStatus;



function getBraintreeToken(req, res) {
    logging.startSection("get_braintree_token");
    logging.logRequest(req);


    gateway.clientToken.generate({}, function (err, response) {
        logging.logResponse(response);
        res.send(JSON.stringify(response));
    });
};


function createCustomer(firstName,lastName, nounce, callback){
        gateway.customer.create({
            firstName: firstName,
            lastName: lastName,
            creditCard: {
                paymentMethodNonce: nounce,
                options: {
                    verifyCard: true,
                    //failOnDuplicatePaymentMethod: true,
                    makeDefault: true
                }
            }
        }, function (err, result) {

            if(err){
                return callback(err,result);
            }

            if(result.success == true){
                return callback(null,result);
            }
            else{
                return callback(null,result);
            }
        });
}


function addCreditCard(req,res){
    logging.startSection("add_credit_card");
    logging.logRequest(req);

    var accessToken     = req.body.access_token;
    var nounce          = req.body.nounce;

    var manValues   = [accessToken,nounce];
    var isBlank     = utils.checkBlank(manValues);
    if (isBlank == 1)
    {
        responses.parameterMissingResponse(res);
        return;
    }

    utils.authenticateUser(accessToken, function (user){
        if (user == 0)
        {
            responses.authenticationErrorResponse(res);
            return;
        }
        var userId = user[0].user_id;
        var userName = user[0].user_name;
        var sql ="SELECT `braintree_customer_id` FROM `tb_users` WHERE `user_id`=? LIMIT 1";
        connection.query(sql,[userId],function(err,result){
            logging.logDatabaseQuery("Get existing braintree_customer_id", err, result, null);
            if(result[0].braintree_customer_id === ''){
                createCustomer(userName, userName, nounce, function(err,result){
                    if (err) {
                        responses.sendError(res);
                        return;
                    }
                    if (result.success == false) {
                        var response = {
                            "error": result.message,
                            "flag": constants.responseFlags.DUPLICATE_CARD
                        };
                        logging.logResponse(response);
                        res.send(JSON.stringify(response));
                        return;
                    }

                    var sql = "UPDATE `tb_users` SET `credit_card_flag`=?,`braintree_customer_id`=? WHERE `user_id`=? LIMIT 1";
                    connection.query(sql,[1,result.customer.id,userId],function(err,result){
                        logging.logDatabaseQuery("Updating braintree_customer_id for user", err, result, null);
                    });

                    var sql = "INSERT INTO `tb_user_credit_cards`(`user_id`, `card_token`, `last4_digits`, `default_card`) VALUES (?,?,?,?)";
                    connection.query(sql,[userId,result.customer.creditCards[0].token,result.customer.creditCards[0].verification.creditCard.last4,1],function(err,result){
                        logging.logDatabaseQuery("Credit card added", err, result, null);
                        if(err){
                            responses.sendError(res);
                            return;
                        }
                        var response = {
                            "log": "Card added successfully"
                        };
                        logging.logResponse(response);
                        res.send(JSON.stringify(response));
                    });

                });
            }
            else{
                gateway.creditCard.create({
                    customerId: result[0].braintree_customer_id,
                    paymentMethodNonce: nounce,
                    options: {
                        verifyCard: true,
                        //failOnDuplicatePaymentMethod: true,
                        makeDefault: true
                    }
                }, function (err, result) {

                    if (err) {
                        responses.sendError(res);
                        return;
                    }

                    if (result.success == false) {
                        var response = {
                            "error": result.message,
                            "flag": constants.responseFlags.DUPLICATE_CARD
                        };
                        logging.logResponse(response);
                        res.send(JSON.stringify(response));
                        return;
                    }

                    var sql = "UPDATE `tb_user_credit_cards` SET `default_card`=? WHERE `user_id`=? && `default_card`=? LIMIT 1";
                    connection.query(sql,[0,userId,1],function(err,resultChangeDefaultCard){
                        logging.logDatabaseQuery("Changing default status of existing default card", err, result, null);
                        var sql = "INSERT INTO `tb_user_credit_cards`(`user_id`, `card_token`, `last4_digits`, `default_card`) VALUES (?,?,?,?)";
                        connection.query(sql,[userId,result.creditCard.token,result.creditCard.last4,1],function(err,result){
                            logging.logDatabaseQuery("Inserting new credit card", err, result, null);
                            var response = {
                                "log": "Card added successfully"
                            };
                            logging.logResponse(response);
                            res.send(JSON.stringify(response));
                        });
                    });

                });
            }
        });
    })
}


function listCreditCards(req,res){
    logging.startSection("list_credit_cards");
    logging.logRequest(req);

    var accessToken     = req.body.access_token;

    var manValues   = [accessToken];
    var isBlank     = utils.checkBlank(manValues);
    if (isBlank == 1)
    {
        responses.parameterMissingResponse(res);
        return;
    }

    utils.authenticateUser(accessToken, function (user){
        if (user == 0)
        {
            responses.authenticationErrorResponse(res);
            return;
        }

        var userId = user[0].user_id;

        var sql ="SELECT `credit_card_flag` FROM `tb_users` WHERE `user_id`=? LIMIT 1";
        connection.query(sql,[userId],function(err,result){
            logging.logDatabaseQuery("Get existing braintree_customer_id", err, result, null);

            if(result[0].credit_card_flag === 1){

                var sql = "SELECT `id`,`last4_digits`,`default_card`,`card_added_on` FROM `tb_user_credit_cards` WHERE `user_id`=?";
                connection.query(sql,[userId],function(err,result){

                    logging.logDatabaseQuery("Get existing credit cards of user", err, result, null);
                    if (err) {
                        responses.sendError(res);
                        return;
                    }

                    var length = result.length;

                    for(var i=0;i<length;i++){
                        result[i].card_added_on = result[i].card_added_on.toISOString().replace(/T/, ' ').replace(/\..+/, '');
                    }

                    var response = {
                        "list_credit_cards": result
                    };
                    logging.logResponse(response);
                    res.send(JSON.stringify(response));

                });
            }
            else{
                var response = {
                    "list_credit_cards": []
                };
                logging.logResponse(response);
                res.send(JSON.stringify(response));
            }
        });
    })
}


function ChangeDefaultCreditCard(req,res){
    logging.startSection("change_default_credit_card");
    logging.logRequest(req);

    var accessToken     = req.body.access_token;
    var cardId          = req.body.card_id;

    var manValues   = [accessToken,cardId];
    var isBlank     = utils.checkBlank(manValues);
    if (isBlank == 1)
    {
        responses.parameterMissingResponse(res);
        return;
    }

    utils.authenticateUser(accessToken, function (user){
        if (user == 0)
        {
            responses.authenticationErrorResponse(res);
            return;
        }

        var userId = user[0].user_id;

        var sql = "UPDATE `tb_user_credit_cards` SET `default_card`=? WHERE `user_id`=? && `default_card`=? LIMIT 1";
        connection.query(sql,[0,userId,1],function(err,resultChangeDefaultCard){

            logging.logDatabaseQuery("Changing default status of existing default card", err, resultChangeDefaultCard, null);

            if (err) {
                responses.sendError(res);
                return;
            }

            var sql = "UPDATE `tb_user_credit_cards` SET `default_card`=? WHERE `id`=? LIMIT 1";
            connection.query(sql,[1,cardId],function(err,result){

                logging.logDatabaseQuery("Set credit card as default", err, result, null);

                if (err) {
                    responses.sendError(res);
                    return;
                }

                var response = {
                    "log": "Default credit card updated successfully"
                };
                logging.logResponse(response);
                res.send(JSON.stringify(response));
            });
        });

    })
}


function DeleteCreditCard(req,res){
    logging.startSection("delete_credit_card");
    logging.logRequest(req);

    var accessToken     = req.body.access_token;
    var cardId          = req.body.card_id;

    var manValues   = [accessToken,cardId];
    var isBlank     = utils.checkBlank(manValues);
    if (isBlank == 1)
    {
        responses.parameterMissingResponse(res);
        return;
    }

    utils.authenticateUser(accessToken, function (user) {
        if (user == 0) {
            responses.authenticationErrorResponse(res);
            return;
        }

        var userId = user[0].user_id;


        var sql = "SELECT count(`id`) AS count FROM `tb_user_credit_cards` LIMIT 1";
        connection.query(sql, function (err, result) {

            logging.logDatabaseQuery("Number of credit cards of a user", err, result, null);

            if (err) {
                responses.sendError(res);
                return;
            }

            if(result[0].count === 1){
                var response = {
                    "error": "Deletion of last credit card from your account is not allowed"
                };
                logging.logResponse(response);
                res.send(JSON.stringify(response));
                return;
            }

            var sql = "DELETE FROM `tb_user_credit_cards` WHERE `id`=? && `default_card`=? LIMIT 1";
            connection.query(sql, [cardId,0], function (err, result) {

                logging.logDatabaseQuery("Deleting credit card", err, result, null);

                if (err) {
                    responses.sendError(res);
                    return;
                }

                if(result.affectedRows === 0){
                    var response = {
                        "error": "Default credit card cannot be deleted"
                    };
                    logging.logResponse(response);
                    res.send(JSON.stringify(response));
                    return;
                }

                var response = {
                    "log": "Credit card deleted successfully"
                };
                logging.logResponse(response);
                res.send(JSON.stringify(response));
            });
        });

    });
}




function paymentOnRideCompletion(req, res) {
    logging.startSection("payment_on_ride_completion");
    logging.logRequest(req);

    var accessToken = req.body.access_token;
    var engagementId = req.body.engagement_id;
    var paymentMethod = req.body.payment_method; // 0 - cash / 1- card
    var amountToPay = req.body.to_pay;
    var tip = req.body.tip;


    var manValues = [accessToken, engagementId, paymentMethod, amountToPay, tip];
    var isBlank = utils.checkBlank(manValues);
    if (isBlank == 1) {
        responses.parameterMissingResponse(res);
        return;
    }

    utils.authenticateUser(accessToken, function(user) {
        if (user == 0) {
            responses.authenticationErrorResponse(res);
            return;
        }

        var userId = user[0].user_id;
        var IsPaymentSuccessful = 0;

        var sql = "SELECT `driver_id` FROM `tb_engagements` WHERE `engagement_id`=? LIMIT 1";
        connection.query(sql, [engagementId], function(err, result) {
            if (err) {
                responses.sendError(res);
                return;
            }
            var driverId = result[0].driver_id;

            if (paymentMethod == 0) { // Cash

                CheckPaymentStatusOnEndRide(engagementId, function(err, users) {
                    if (err) {
                        responses.sendError(res);
                        return;
                    }

                    var length = users.length;

                    for (var i = 0; i < length; i++) {
                        if (users[i].user_id == userId) {


                            if (users[i].defaulter_flag == 1) {
                                var response = {
                                    "error": "Please make payment by card",
                                    "flag": constants.responseFlags.SHOW_ERROR_MESSAGE
                                };
                                logging.logResponse(response);
                                res.send(JSON.stringify(response));
                                return;
                            }
                            IsPaymentSuccessful = 1;
                            var sql = "UPDATE `tb_engagements` SET `tip`=?,`is_payment_successful`=?,`payment_method`=? WHERE `engagement_id`=? LIMIT 1";
                            connection.query(sql, [tip, IsPaymentSuccessful, paymentMethod, engagementId], function(err, result) {
                                logging.logDatabaseQuery("Updating tip and is_payment_successful fields in engagement info", err, result, null);

                                if (err) {
                                    responses.sendError(res);
                                    return;
                                }

                                var response = {
                                    "log": "Please make cash payment to driver"
                                };
                                logging.logResponse(response);
                                res.send(JSON.stringify(response));

                                if (paymentMethod == 0) {
                                    var driverCashEarning = amountToPay;
                                }

                                var sql = "UPDATE `tb_users` SET `todays_earnings`=`todays_earnings`+?,`total_earnings`= `total_earnings`+?,`total_money_owed`=`total_money_owed`+? , driver_cash_earning = driver_cash_earning + ? WHERE `user_id`=? LIMIT 1";
                                connection.query(sql, [tip, tip, tip, driverCashEarning, driverId], function(err, result) {
                                    logging.logDatabaseQuery("Updating driver earnings", err, result, null);
                                });
                                var sql = "UPDATE `tb_users` SET `todays_money_spent`=`todays_money_spent`+?,`total_money_spent`= `total_money_spent`+? WHERE `user_id`=? LIMIT 1";
                                connection.query(sql, [amountToPay, amountToPay, userId], function(err, result) {
                                    logging.logDatabaseQuery("Updating customer spending", err, result, null);
                                });

                                //sendPushToDriverToCollectPayableAmountFromCustomer(engagementId,paymentMethod,amountToPay);
                                return;
                            });
                        }
                    }

                });

            }
            if (paymentMethod == 1) { // Card
                // error in payment from card case pending
                // Make payment
                // send push to driver to indicate payment done from customer
                // add tip, update payment mode in engagement

                var sql = "SELECT a.braintree_customer_id,b.card_token FROM tb_users a,tb_user_credit_cards b WHERE a.user_id=b.user_id  && b.default_card = ? && a.user_id=? LIMIT 1";
                connection.query(sql, [1, userId], function(err, result) {

                    logging.logDatabaseQuery("Getting existing braintree_customer_id and default card for card payment", err, result, null);
                    if (err) {
                        responses.sendError(res);
                        return;
                    }

                    if (result.length === 0) {
                        var response = {
                            "error": "No credit card found in your account",
                            "flag": constants.responseFlags.SHOW_ERROR_MESSAGE
                        };
                        logging.logResponse(response);
                        res.send(JSON.stringify(response));
                        return;
                    }

                    if (parseFloat(amountToPay) == 0.0) {

                        IsPaymentSuccessful = 1;
                        var response = {
                            "log": "Payment Successful"
                        };
                        logging.logResponse(response);
                        res.send(JSON.stringify(response));

                        var sql = "UPDATE `tb_engagements` SET `is_payment_successful`=?,`payment_method`=? WHERE `engagement_id`=? LIMIT 1";
                        connection.query(sql, [IsPaymentSuccessful, paymentMethod, engagementId], function(err, result) {
                            logging.logDatabaseQuery("Updating is_payment_successful fields in engagement info", err, result, null);

                            //sendPushToDriverToCollectPayableAmountFromCustomer(engagementId,paymentMethod,amountToPay);

                        });

                        return;
                    }

                    gateway.transaction.sale({
                        customerId: result[0].braintree_customer_id,
                        amount: amountToPay,
                        paymentMethodToken: result[0].card_token,
                        options: {
                            submitForSettlement: true
                        }
                    }, function(err, result) {

                        console.log(err)
                        console.log(result)

                        if (err) {
                            var response = {
                                "error": "Error in payment",
                                "flag": constants.responseFlags.SHOW_ERROR_MESSAGE
                            };
                            logging.logResponse(response);
                            res.send(JSON.stringify(response));
                            return;
                        }

                        if (result.success !== true) {
                            var response = {
                                "error": "Error in payment",
                                "flag": constants.responseFlags.SHOW_ERROR_MESSAGE
                            };
                            logging.logResponse(response);
                            res.send(JSON.stringify(response));
                            return;
                        }

                        if (result.success === true) {
                            IsPaymentSuccessful = 1;
                            var response = {
                                "log": "Payment Successful"
                            };
                            logging.logResponse(response);
                            res.send(JSON.stringify(response));
                        }

                        var sql = "UPDATE `tb_engagements` SET `tip`=?,`is_payment_successful`=?,`payment_method`=?,`transaction_id`=? WHERE `engagement_id`=? LIMIT 1";
                        connection.query(sql, [tip, IsPaymentSuccessful, paymentMethod, result.transaction.id, engagementId], function(err, result) {
                            logging.logDatabaseQuery("Updating tip and is_payment_successful fields in engagement info", err, result, null);

                            var sql = "UPDATE `tb_users` SET `todays_earnings`=`todays_earnings`+?,`total_earnings`= `total_earnings`+?,`total_money_owed`=`total_money_owed`+? WHERE `user_id`=? LIMIT 1";
                            connection.query(sql, [tip, tip, tip, driverId], function(err, result) {
                                logging.logDatabaseQuery("Updating driver earnings", err, result, null);
                            });

                            var sql = "UPDATE `tb_users` SET `todays_money_spent`=`todays_money_spent`+?,`total_money_spent`= `total_money_spent`+? WHERE `user_id`=? LIMIT 1";
                            connection.query(sql, [amountToPay, amountToPay, userId], function(err, result) {
                                logging.logDatabaseQuery("Updating customer spending", err, result, null);
                            });

                            //sendPushToDriverToCollectPayableAmountFromCustomer(engagementId,paymentMethod,amountToPay);

                        });


                    });


                });


            }


        });
    });
}


function sendPushToDriverToCollectPayableAmountFromCustomer(engagementId,paymentMethod,amountToPay){

    var sql = "SELECT `driver_id` FROM `tb_engagements` WHERE `engagement_id`=? LIMIT 1";
    connection.query(sql,[engagementId],function(err,result){

        logging.logDatabaseQuery("Getting driver_id for engagement", err, result, null);
        var driverId = result[0].driver_id;

        var message = "Please collect "+amountToPay+" in cash from customer";

        if(paymentMethod == 1){         // By card
            message = "Payment of "+amountToPay+" has been collected from customer";
        }

        var flag = constants.notificationFlags.PAYMENT_INFO_TO_DRIVER;

        var payload = {"engagement_id": engagementId, "paymentMethod": paymentMethod, "amountToPay": amountToPay, "flag": flag};

        pushNotification.sendNotification(driverId, message, flag, payload);
    });

}


function checkPaymentStatus(req, res){

    logging.startSection("check_payment_status");
    logging.logRequest(req);

    var accessToken = req.body.access_token;
    var engagementId = req.body.engagement_id;

    var manValues   = [accessToken,engagementId];
    var isBlank     = utils.checkBlank(manValues);
    if (isBlank == 1)
    {
        responses.sendParameterMissingError(res);
        return;
    }

    utils.authenticateUser(accessToken, function(result) {
        if(result == 0){
            var response = {
                flag    : constants.responseFlags.INVALID_ACCESS_TOKEN,
                error   : 'Invalid access token'};
            logging.logResponse(response);
            res.send(response);
            return;
        }

        CheckPaymentStatusOnEndRide(engagementId, function(err, users){
            if(err){
                responses.sendError(res);
                return;
            }

            var length = users.length;

            for(var i = 0;i < length; i++){
                delete users[i]['driver_id'];
                delete users[i]['driver_rating'];
                delete users[i]['skip_rating_by_customer'];
                delete users[i]['car_type'];
                delete users[i]['discount'];
                delete users[i]['distance_travelled'];
                delete users[i]['ride_time'];
                delete users[i]['wait_time'];
                delete users[i]['split_fare_key'];
                delete users[i]['session_id'];
                delete users[i]['account_id'];
            }

            if(result[0].reg_as == 1){
                logging.logResponse({"users":users});
                res.send({"users":users});
            }
            else{
                var length = users.length;
                var userId = result[0].user_id;
                for(var i =0; i< length; i++){
                    if(users[i].user_id == userId){
                        logging.logResponse({"users":users[i]});
                        res.send({"users":users});
                        return;
                    }
                }
            }

        });

    });

}


exports.CheckPaymentStatusOnEndRide = CheckPaymentStatusOnEndRide;

function CheckPaymentStatusOnEndRide(engagementId,callback){
    setDefaulterFlag(engagementId, function(err,result){
        if(err){
            return callback(err,0);
        }
        ride_module.getCustomersInvolvedInEngagement(engagementId, function(err,users){
            if(err){
                return callback(err,0);
            }
            return callback(null,users);
        });
    });
}


exports.setDefaulterFlag = setDefaulterFlag;

function setDefaulterFlag(engagementId, callback){

    var sql = "SELECT TIMESTAMPDIFF(MINUTE,`drop_time`,NOW()) AS time_diff_minutes,`payment_method`,`is_payment_successful`,";
    sql +="`defaulter_flag`,`engagement_id`,`user_id` FROM `tb_engagements` WHERE `split_fare_key` = ";
    sql +="(SELECT `split_fare_key` FROM `tb_engagements` WHERE `engagement_id` = ? LIMIT 1)";
    connection.query(sql, [engagementId], function(err,result){
        logging.logDatabaseQuery("Get engagement's payment details to update defaulter flag", err, result, null);
        if(err){
            return callback(err,0);
        }

        var length = result.length;
        var arr_engagements = [];

        if(!result[0].time_diff_minutes){
            return callback(null,1);
        }

        if(result[0].time_diff_minutes >= 1){

            var message = "Please make payment by card";
            var flag = constants.notificationFlags.HIDE_CASH_PAYMENT;


            for(var i = 0; i < length; i++){
                if(result[i].is_payment_successful == 0 && result[i].defaulter_flag == 0){

                    arr_engagements.push(result[i].engagement_id);

                    var payload = {"engagement_id": engagementId, "defaulter_flag" : 1, "flag": flag};

                    pushNotification.sendNotification(result[i].user_id, message, flag, payload);
                }
            }

            var str = -1;
            if(arr_engagements.length > 0){
                str = arr_engagements.toString();
            }

            var sql = "UPDATE `tb_engagements` SET `payment_method`=?, `defaulter_flag`=? WHERE `engagement_id` IN("+str+")";
            connection.query(sql,[constants.PaymentMethod.CARD, 1], function(err,result){
                logging.logDatabaseQuery("Update defaulter flag", err, result, null);
                if(err){
                    return callback(err,0);
                }
                return callback(null,1);
            });
        }

    });
}