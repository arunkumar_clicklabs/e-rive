var fs = require('fs');
var logging = require('./logging');
var constants = require('./constants');
var utils       = require('./../utils/commonfunction');

exports.upload_file = function(req, res){
    logging.startSection("Upload a file to the server");
    logging.logRequest(req);

    console.log('---------------------------------------');
    console.log('| PARAMETERS WHEN UPLOADING THE FILES |');
    console.log('---------------------------------------');
    console.log(JSON.stringify(req.body));
    console.log(JSON.stringify(req.files));

    var fileType = req.body.file_type;

    if( fileType == constants.FILE_TYPE.RIDE_PATH){

        utils.uploadImageFileToS3Bucket(req.files.ride_path, config.get('s3BucketCredentials.folder.rideData'), function(result_file) {
            if (result_file != 0){
                res.send("An error occurred in saving the file");
                return;
            }

            var s3path = config.get('s3BucketCredentials.s3URL')+'/'+config.get('s3BucketCredentials.folder.rideData')+'/'+result_file;
            console.log(s3path)
            res.send("The file was saved to " + s3path);
        });
    }
    else{
        res.send("Nothing to upload");
    }

};