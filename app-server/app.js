
/**
 * Module dependencies.
 */

var path            = require('path');



process.env.NODE_ENV = 'development';



if(__dirname == "/Users/clicklabs68/Documents/untitled folder 2/base-taxihawk-server/app-server")
{
	process.env.NODE_ENV = 'localDevelopment';
}

process.env.NODE_CONFIG_DIR = path.join(__dirname, 'config');






basePathApp = __dirname;
console.log(basePathApp+"   ============basepathapp");
// process.env.NODE_ENV = 'localDevelopment';
// process.env.NODE_CONFIG_DIR = '/config';

//process.env.NODE_ENV = 'development';
//process.env.NODE_CONFIG_DIR = '/home/storx/taximust/taximust/config';

//process.env.NODE_ENV = 'distribution';        // For testing
//process.env.NODE_CONFIG_DIR = '/home/storx/taximust/taximust-test/config';

require('newrelic'); // Configure the App Name in the newrelic.js file

//process.env.NODE_ENV = 'development';
//process.env.NODE_CONFIG_DIR = '/config';

//process.env.NODE_ENV = 'review';
//process.env.NODE_CONFIG_DIR = '/home/taxihawk/taximust-app/config';


console.log("=======================Environment")
console.log(process.env.NODE_ENV);

config = require('config');


var express         = require('express');
var http            = require('http');
var path            = require('path');
var bodyParser      = require('body-parser');
var favicon         = require('serve-favicon');
var errorhandler    = require('errorhandler');
var logger          = require('morgan');
var methodOverride  = require('method-override');

var multipart       = require('connect-multiparty');
var multipartMiddleware = multipart();

connection          = require('./routes/connection');
var server          = require('./routes/server');
var ride_parallel_dispatcher = require('./routes/ride_parallel_dispatcher');
var manual_dispatcher = require('./routes/manual_dispatcher');
var userlogin       = require('./routes/user_login');
var rating          = require('./routes/rating');
var testing         = require('./routes/testing');
var service         = require('./routes/service');
var crone           = require('./routes/crone');
var user            = require('./routes/user');
var mailer          = require('./routes/mailer');
var messenger       = require('./routes/messenger');
var analytics       = require('./routes/analytics');
var heartbeat       = require('./routes/heartbeat');
var driver          = require('./routes/driver');
var driverLogin     = require('./routes/driver_login');
var make_payments   = require('./routes/make_payments');
var split_fare      = require('./routes/split_fare');
var paypal_payment      = require('./routes/paypal_payment');
var devapi = require('./routes/devapi');

var app = express();

// all environments
app.set('port', process.env.PORT || config.get('PORT'));
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
app.use(favicon(__dirname + '/views/favicon.ico'));
app.use(logger('dev'));
app.use(methodOverride());
app.use(express.static(path.join(__dirname, 'public')));

app.get('/apitest', function (req, res) {
    res.render('test');
});
app.get('/devapi', function (req, res) {
    res.render('devapi');
});

// Customer Login / signup APIs ===============================================================

app.use('/customer_registeration',          multipartMiddleware);
app.post('/customer_registeration',         userlogin.register_a_user);
app.post('/email_login',                    userlogin.email_login);
app.post('/customer_fb_registeration_form', userlogin.login_fb);
app.post('/start_app_using_access_token',   userlogin.start_app_using_access_token);
app.post('/logout',                         userlogin.logout);
app.post('/forgot_password',                userlogin.forgotPasswordFromEmail);


// Customer profile APIs ===============================================================

app.post('/user_profile_information',       userlogin.UserProfileInformation);
app.use('/edit_customer_profile',           multipartMiddleware);
app.post('/edit_customer_profile',          userlogin.editCustomerProfile);
app.post('/reset_password',                 userlogin.resetPassword);


// Customer specific APIs ===============================================================

app.post('/get_missed_rides',               user.get_missed_rides);
app.post('/booking_history',                user.booking_history);
app.post('/get_driver_current_location',    user.get_driver_current_location);
//app.post('/send_otp_via_call',              userlogin.send_otp_via_call);


// Driver Login / signup APIs =====================================================================

app.post('/login_driver_via_email',             driverLogin.emailLogin);
app.post('/login_driver_via_access_token',      driverLogin.accessTokenLogin);
app.post('/logout_driver',                      driverLogin.logout);


// Driver profile APIs ===============================================================

app.use('/edit_driver_profile',                 multipartMiddleware);
app.post('/edit_driver_profile',                driverLogin.editDriverProfile);


// Driver specific APIs ===============================================================

app.post('/change_availability',			    driver.changeAvailability);
app.post('/update_driver_location',             driver.updateLocation);
app.post('/acknowledge_heartbeat',              heartbeat.acknowledge_heartbeat);


// Dispatcher APIs  ==========================================

app.post('/find_a_driver',                  ride_parallel_dispatcher.find_driver_in_area);
app.post('/request_ride',                   ride_parallel_dispatcher.request_ride);
app.post('/acknowledge_request', 			ride_parallel_dispatcher.acknowledge_request);
app.post('/acknowledge_manual_engagement', 	ride_parallel_dispatcher.acknowledge_manual_engagement);
app.post('/accept_a_request',               ride_parallel_dispatcher.accept_a_request);
app.post('/update_manual_destination',      userlogin.updateManualDestinationInfo);         // By customer after confirmation of ride acceptance
app.post('/reject_a_request',               ride_parallel_dispatcher.reject_a_request);
app.post('/cancel_the_request',             ride_parallel_dispatcher.cancel_the_request);
app.post('/cancel_the_ride',                ride_parallel_dispatcher.cancel_the_ride);
app.post('/start_ride',                     ride_parallel_dispatcher.start_ride);
app.post('/end_ride',                       ride_parallel_dispatcher.end_ride);
app.post('/make_driver_free',               ride_parallel_dispatcher.makeDriverFree);
app.post('/get_current_user_status',        ride_parallel_dispatcher.get_current_user_status);
app.post('/update_in_ride_data',            ride_parallel_dispatcher.captureRideData);
app.post('/create_manual_engagement',       ride_parallel_dispatcher.create_manual_engagement);
app.post('/clear_engagements_for_driver',   ride_parallel_dispatcher.clear_engagements_for_driver);
app.post('/clear_engagements_for_customer', ride_parallel_dispatcher.clear_engagements_for_customer);


// Scheduling APIs   ===============================================================

app.post('/insert_pickup_schedule',     manual_dispatcher.insert_pickup_schedule);
app.post('/show_pickup_schedules',      manual_dispatcher.show_pickup_schedules);
app.post('/modify_pickup_schedule',     manual_dispatcher.modify_pickup_schedule);
app.post('/remove_pickup_schedule',     manual_dispatcher.remove_pickup_schedule);


// Discounts and coupons    ===============================================================

app.post('/get_coupons',            user.get_coupons);
app.post('/enter_code',             user.enter_code);

// Information for the app  ===============================================================

app.post('/get_information',        service.get_information);


// File uploads ===============================================================

var uploads = require('./routes/uploads');
app.use('/upload_file',     multipartMiddleware);
app.post('/upload_file',    uploads.upload_file);


// Split fare APIs  ===============================================================

app.post('/send_split_fare_request',            split_fare.sendSplitFareRequest);
app.post('/split_fare_screen',                  split_fare.splitFareScreen);
app.post('/user_response_to_split_fare_request',split_fare.userResponseToSplitFareRequest);
app.post('/resend_split_fare_request',          split_fare.resendSplitFareRequest);


// Make Payment APIs  ===============================================================

app.post('/get_braintree_token',               make_payments.getBraintreeToken);
app.post('/add_credit_card',                   make_payments.addCreditCard);
app.post('/list_credit_cards',                 make_payments.listCreditCards);
app.post('/change_default_credit_card',        make_payments.ChangeDefaultCreditCard);
app.post('/delete_credit_card',                make_payments.DeleteCreditCard);
app.post('/payment_on_ride_completion',        make_payments.paymentOnRideCompletion);
app.post('/check_payment_status',              make_payments.checkPaymentStatus);
app.get('/paypal_payment',                    paypal_payment.paypalPayment);


// APIs for rating  =========================================================================

//app.post('/rating',                         rating.rating);
app.post('/rate_the_driver',                rating.rateTheDriver);
app.post('/skip_rating_by_customer',        rating.skipRatingByCustomer);


// APIs for the crones running  ==============================================================

app.get('/remove_inactive_engagements',         crone.removeInactiveEngagements);
app.get('/crone_to_update',                     crone.update_lat_long_of_driver_to_zero);
app.get('/schedule_request_if_any',             manual_dispatcher.schedule_request_if_any);

// Testing services ===============================================================

app.post('/test_sending_sms',           testing.test_sending_sms);                                          //support
app.post('/test_email_service',         testing.test_email_service);                                        //support
app.post('/check_push_notification',    testing.check_push_notification);                                    //support

// Analytics    ===============================================================

//app.post('/generate_report_for_driver', analytics.generate_report_for_driver);
//app.post('/get_location_updates',       analytics.get_location_updates);
//app.get('/get_data_for_rides',          analytics.get_data_for_rides);
//app.get('/get_revenues',                analytics.get_revenues);
//app.get('/get_demographic_rides_data',  analytics.get_demographic_rides_data);
//app.get('/get_device_rides_data',       analytics.get_device_rides_data);
//app.get('/get_user_registration_data',  analytics.get_user_registration_data);
//app.get('/service_quality_data',        analytics.service_quality_data);
//app.get('/hourly_service_quality_data', analytics.hourly_service_quality_data);
//app.get('/rides_completed_today',   analytics.rides_completed_today);
//app.get('/rides_today',             analytics.rides_today);
//app.get('/users_registered_today',  analytics.users_registered_today);
//
// dev apis ===============================================================



// if(process.env.NODE_ENV == "development")
// {
app.post('/makeMeFree',                 devapi.makeMeFree);
app.post('/clearLastEngagement',        devapi.clearLastEngagement);
app.post('/getSomething',               devapi.getSomething);
app.post('/makeMeDriver',               devapi.makeMeDriver);
app.post('/updateCarType',               devapi.updateCarType);
//}


http.createServer(app).listen(app.get('port'), function(){
    console.log('Express server listening on port ' + app.get('port'));
});
