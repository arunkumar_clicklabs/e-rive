var logging = require('./../routes/logging');
var constants = require('./../routes/constants');
var debugging_enabled = true;


/**
 * Send notification to the user with the given user ID with ASSUMPTION: The payload is same for both the devices
 * @param user_id
 * @param message
 * @param flag
 * @param payload
 */
exports.sendNotification = function(user_id, message, flag, payload)
{
    console.log("SENDING NOTIFICATION: " + message + " TO: " + user_id);
    var get_user_device_info = "SELECT `user_id`,`current_user_status`,`device_type`,`user_device_token` FROM `tb_users` WHERE `user_id`=?";
    connection.query(get_user_device_info, [user_id], function (err, result_user)
    {
        logging.logDatabaseQuery("Get device information for the driver", err, result_user, null);
        module.exports.sendNotificationToDevice(result_user[0].device_type, result_user[0].user_device_token, message, flag, payload,result_user[0].current_user_status);
    });
};



/**
 *
 * @param deviceType
 * @param userDeviceToken
 * @param message
 * @param flag
 * @param payload
 * @param current_user_status
 */
exports.sendNotificationToDevice = function(deviceType, userDeviceToken, message, flag, payload, current_user_status)
{
    // The user device token can be empty in case of scheduled pickups, hence, the check
    if (deviceType == constants.deviceType.ANDROID && userDeviceToken != '')
    {
        module.exports.sendAndroidPushNotification(userDeviceToken, payload);
    }
    else if (deviceType == constants.deviceType.iOS && userDeviceToken != '')
    {
        if(current_user_status == 1){       // 1- driver
            module.exports.sendIosPushNotificationToDriver(userDeviceToken, message, flag, payload);
        }
        else if(current_user_status == 2){                               // 2-customer
            module.exports.sendIosPushNotification(userDeviceToken, message, flag, payload);
        }
    }
};



/**
 * Send the notification to the android device
 * @param deviceToken
 * @param message
 */

exports.sendAndroidPushNotification = function(deviceToken, message)
{

    console.log(message)
    var gcm = require('node-gcm');
    var message = new gcm.Message({
        delayWhileIdle: false,
        timeToLive: 2419200,
        data: {
            message: message,
            brand_name: config.get('androidPushSettings.brandName')
        }
    });
    var sender = new gcm.Sender(config.get('androidPushSettings.gcmSender'));
    var registrationIds = [];
    registrationIds.push(deviceToken);

    sender.send(message, registrationIds, 4, function(err, result) {
        if(debugging_enabled) {
            console.log("ANDROID NOTIFICATION RESULT: " + JSON.stringify(result));
            console.log("ANDROID NOTIFICATION ERROR: " + JSON.stringify(err));
        }
    });
};


/**
 * sendIosPushNotificationToDriver
 * @param iosDeviceToken
 * @param message
 * @param flag
 * @param payload
 */

exports.sendIosPushNotificationToDriver = function(iosDeviceToken, message, flag, payload)
{
    console.log("===================================================")
    console.log("To driver")
    console.log("iosDeviceToken")
    console.log(iosDeviceToken)

    console.log(config.get('iOSPushSettings.iosApnCertificateForDriver'))
    console.log(config.get('iOSPushSettings.gateway'))

    if(payload.address){
        payload.address='';
    }
    console.log(iosDeviceToken)
    console.log(message)
    console.log(flag)
    console.log("payload")
    console.log(payload)
    var status = 1;
    var msg = message;
    var snd = 'ping.aiff';
    if (flag == 6 || flag == 40)
    {
        status = 0;
        msg = '';
        snd = '';
    }

    var apns = require('apn');
    var deviceToken = new apns.Device(iosDeviceToken);

    // for development

    var options = {
        cert        : config.get('iOSPushSettings.iosApnCertificateForDriver'),
        certData    : null,
        key         : config.get('iOSPushSettings.iosApnCertificateForDriver'),
        keyData     : null,
        passphrase  : 'click',
        ca          : null,
        pfx         : null,
        pfxData     : null,
        gateway     : config.get('iOSPushSettings.gateway'),
        port        : 2195,
        rejectUnauthorized: true,
        enhanced    : true,
        cacheLength : 100,
        autoAdjustCache: true,
        connectionTimeout: 0,
        ssl         : true
    };



    var apnsConnection = new apns.Connection(options);
    var note = new apns.Notification();

    note.expiry = Math.floor(Date.now() / 1000) + 3600;
    note.contentAvailable = 1;
    note.sound = snd;
    note.alert = msg;
    note.newsstandAvailable = status;
    note.payload = payload;

    apnsConnection.pushNotification(note, deviceToken);

    // Handle these events to confirm that the notification gets
    // transmitted to the APN server or find error if any
    function log(type) {
        return function() {
            if(debugging_enabled)
                console.log("iOS development PUSH NOTIFICATION RESULT: " + type);
        }
    }

    apnsConnection.on('error', log('error'));
    apnsConnection.on('transmitted', log('transmitted'));
    apnsConnection.on('timeout', log('timeout'));
    apnsConnection.on('connected', log('connected'));
    apnsConnection.on('disconnected', log('disconnected'));
    apnsConnection.on('socketError', log('socketError'));
    apnsConnection.on('transmissionError', log('transmissionError'));
    apnsConnection.on('cacheTooSmall', log('cacheTooSmall'));

};

/**
 * Send the notification to the iOS device for customer
 * @param iosDeviceToken
 * @param message
 * @param flag
 * @param payload
 */

exports.sendIosPushNotification = function(iosDeviceToken, message, flag, payload)
{
    console.log("To customer")
    console.log("payload")
    console.log(payload)

    console.log("flag")
    console.log(flag)

    console.log(config.get('iOSPushSettings.iosApnCertificateForCustomer'))
    console.log(config.get('iOSPushSettings.gateway'))

    if(payload.address){
        payload.address='';
    }
    var status = 1;
    var msg = message;
    var snd = 'ping.aiff';
    if (flag == 6 || flag == 40)
    {
        status = 0;
        msg = '';
        snd = '';
    }

    var apns = require('apn');

    var options = {
        cert        : config.get('iOSPushSettings.iosApnCertificateForCustomer'),
        certData    : null,
        key         : config.get('iOSPushSettings.iosApnCertificateForCustomer'),
        keyData     : null,
        passphrase  : 'click',
        ca          : null,
        pfx         : null,
        pfxData     : null,
        gateway     : config.get('iOSPushSettings.gateway'),
        port        : 2195,
        rejectUnauthorized: true,
        enhanced    : true,
        cacheLength : 100,
        autoAdjustCache: true,
        connectionTimeout: 0,
        ssl         : true
    };


    var deviceToken = new apns.Device(iosDeviceToken);
    var apnsConnection = new apns.Connection(options);
    var note = new apns.Notification();

    note.expiry = Math.floor(Date.now() / 1000) + 3600;
    note.contentAvailable = 1;
    note.sound = snd;
    note.alert = msg;
    note.newsstandAvailable = status;
    note.payload = payload;

    apnsConnection.pushNotification(note, deviceToken);

    // Handle these events to confirm that the notification gets
    // transmitted to the APN server or find error if any
    function log(type) {
        return function() {
            if(debugging_enabled)
                console.log("iOS PUSH NOTIFICATION RESULT: " + type);
        }
    }

    apnsConnection.on('error', log('error'));
    apnsConnection.on('transmitted', log('transmitted'));
    apnsConnection.on('timeout', log('timeout'));
    apnsConnection.on('connected', log('connected'));
    apnsConnection.on('disconnected', log('disconnected'));
    apnsConnection.on('socketError', log('socketError'));
    apnsConnection.on('transmissionError', log('transmissionError'));
    apnsConnection.on('cacheTooSmall', log('cacheTooSmall'));

};
