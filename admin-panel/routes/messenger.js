/* Module for sending message using Twilio */

var request = require('request');

var logging     = require('./logging');
var constants   = require('./constants');
var utils       = require('./commonfunction');


var client = undefined;
exports.sendMessageByTwillio = sendMessageByTwillio;

function getClient(){
    var accountSid = config.get('twillioCredentials.accountSid');
    var authToken = config.get('twillioCredentials.authToken');

    //require the Twilio module and create a REST client
    client = require('twilio')(accountSid, authToken);
}

function sendMessageByTwillio(to, msg){
    if(client === undefined){
        getClient();
    }
    client.messages.create(
        {to: to, from: config.get('twillioCredentials.fromNumber'), body: msg},
        function(err, message) {
            console.log("Twilio error: " + JSON.stringify(err));
            console.log("Twilio message: " + JSON.stringify(message));
            //if(err){
            //    console.log("Error in sending message: " + JSON.stringify(err));
            //}
        }
    );
}

exports.sendApproveDriverMessage = function(name, to){
    var message = "Dear " + name.split(' ')[0] + ", Congrats!! Your driver account has been approved by"+config.get("projectName")+"'s admin.";
    sendMessageByTwillio(to, message);
};

