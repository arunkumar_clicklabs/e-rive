
var debugging_enabled = true;


exports.logDatabaseQueryError = function (eventFired, error, result)
{
    if(debugging_enabled)
    {
        process.stderr.write("Event: " + eventFired);
        process.stderr.write("\tError: " + JSON.stringify(error));
        process.stderr.write("\tResult: " + JSON.stringify(result));
    }
};