/**
 * The node-module to hold the constants for the server
 */

var myContext = this;

function define(obj, name, value) {
    Object.defineProperty(obj, name, {
        value: value,
        enumerable: true,
        writable: false,
        configurable: true
    });
}

var debugging = false;


exports.responseFlags = {};
exports.responseMessages = {};

//RESPONSE MESSAGES AND FLAGS
exports.refreshMessageAPI = function (req, res) {
    myContext.refreshMessageKeyAndFlag(function (results) {
        res.send(JSON.stringify(results));
    });
}
exports.refreshMessageKeyAndFlag = function (callback) {
    localMessagesAndFlag();
    var sql = "SELECT `message_key`,`message`,`message_flag` FROM `tb_admin_panel_messages`";
    connection.query(sql, [], function (err, result) {
        if (err) {
        } else {
            var messageCount = result.length;
            for (var i = 0; i < messageCount; i++) {
                define(exports.responseFlags, [result[i].message_key], result[i].message_flag);
                define(exports.responseMessages, [result[i].message_key], result[i].message);
            }
            var response = {'log': 'Messages refreshed successfully'};
            return callback(response);
        }
    });


}


function localMessagesAndFlag() {
    //FOR MESSAGES
    define(exports.responseMessages, 'PARAMETER_MISSING',                     'Some parameter missing.');
    define(exports.responseMessages, 'INVALID_ACCESS_TOKEN',                  'Invalid access token.');
    define(exports.responseMessages, 'INVALID_EMAIL_ID',                      'Invalid email id.');
    define(exports.responseMessages, 'INCORRECT_PASSWORD',                     'Incorrect Password.');
    define(exports.responseMessages, 'ACTION_COMPLETE',                       'Action complete.');
    define(exports.responseMessages, 'LOGIN_SUCCESSFULLY',                    'Logged in successfully.');
    define(exports.responseMessages, 'SHOW_ERROR_MESSAGE',                    'Show error message.');
    define(exports.responseMessages, 'SHOW_PROMOCODE_ERROR_MESSAGE',          'Promocode already exists.');
    define(exports.responseMessages, 'IMAGE_FILE_MISSING',                    'Image file is missing.');
    define(exports.responseMessages, 'ERROR_IN_EXECUTION',                    'Error in execution.');
    define(exports.responseMessages, 'DRIVER_NOT_FOUND',                      'Driver not found.');
    define(exports.responseMessages, 'INVALID_DRIVER_FOR_APPROVAL',           'Invalid driver for approval.');
    define(exports.responseMessages, 'INVALID_CAR_TYPE',                      'Invalid car type.');
    define(exports.responseMessages, 'UPLOAD_ERROR',                          'Error in uploading.');
    define(exports.responseMessages, 'DRIVER_APPROVED_SUCCESSFULLY',          'Driver approved successfully.');
    define(exports.responseMessages, 'NOT_A_DRIVER',                          'This user is not a driver.');
    define(exports.responseMessages, 'DRIVER_DECLINED_SUCCESSFULLY',          'Driver declined successfully.');
    define(exports.responseMessages, 'DRIVER_INFO_UPDATED_SUCCESSFULLY',      'Driver info updated successfully.');
    define(exports.responseMessages, 'INVALID_BLOCK_STATUS',                  'Invalid block status.');
    define(exports.responseMessages, 'STATUS_CHANGED_SUCCESSFULLY',           'Status changed successfully.');
    define(exports.responseMessages, 'USER_NOT_FOUND',                        'User not found.');
    define(exports.responseMessages, 'USER_DELETED_SUCCESSFULLY',             'User deleted successfully.');
    define(exports.responseMessages, 'PASSWORD_CHANGED_SUCCESSFULLY',         'Password changed successfully.');
    define(exports.responseMessages, 'INVALID_CAR_ID',                        'Invalid car id.');
    define(exports.responseMessages, 'CAR_FARE_UPDATED_SUCCESSFULLY',         'Car fare updated successfully.');
    define(exports.responseMessages, 'EMAIL_REGISTERED_ALREADY_AS_CUSTOMER',  'Email already registered as customer');
    define(exports.responseMessages, 'EMAIL_REGISTERED_ALREADY_AS_DRIVER',     'Email already registered as driver');
    define(exports.responseMessages, 'EMAIL_ALREADY_EXISTS',           'Email already registered');

    //FOR FLAGS
    define(exports.responseFlags, 'PARAMETER_MISSING',                   100);
    define(exports.responseFlags, 'INVALID_ACCESS_TOKEN',                101);
    define(exports.responseFlags, 'INVALID_EMAIL_ID',                    201);
    define(exports.responseFlags, 'WRONG_PASSWORD',                      201);
    define(exports.responseFlags, 'ACTION_COMPLETE',                     200);
    define(exports.responseFlags, 'LOGIN_SUCCESSFULLY',                  200);
    define(exports.responseFlags, 'SHOW_ERROR_MESSAGE',                  201);
    define(exports.responseFlags, 'IMAGE_FILE_MISSING',                  102);
    define(exports.responseFlags, 'ERROR_IN_EXECUTION',                  404);
    define(exports.responseFlags, 'DRIVER_NOT_FOUND',                    201);
    define(exports.responseFlags, 'INVALID_DRIVER_FOR_APPROVAL',         201);
    define(exports.responseFlags, 'INVALID_CAR_TYPE',                    103);
    define(exports.responseFlags, 'UPLOAD_ERROR',                        201);
    define(exports.responseFlags, 'DRIVER_APPROVED_SUCCESSFULLY',        200);
    define(exports.responseFlags, 'NOT_A_DRIVER',                        201);
    define(exports.responseFlags, 'DRIVER_DECLINED_SUCCESSFULLY',        200);
    define(exports.responseFlags, 'DRIVER_INFO_UPDATED_SUCCESSFULLY',    200);
    define(exports.responseFlags, 'INVALID_BLOCK_STATUS',                104);
    define(exports.responseFlags, 'STATUS_CHANGED_SUCCESSFULLY',         200);
    define(exports.responseFlags, 'USER_NOT_FOUND',                      201);
    define(exports.responseFlags, 'USER_DELETED_SUCCESSFULLY',           200);
    define(exports.responseFlags, 'PASSWORD_CHANGED_SUCCESSFULLY',       200);
    define(exports.responseFlags, 'INVALID_CAR_ID',                      105);
    define(exports.responseFlags, 'CAR_FARE_UPDATED_SUCCESSFULLY',       200);
    return 1;
}

exports.makeMeDriverFlag = {};
define(exports.makeMeDriverFlag, "UNAPPROVED_DRIVER_STATUS",                 1);
define(exports.makeMeDriverFlag, "APPROVED_DRIVER_STATUS",                   0);
define(exports.makeMeDriverFlag, "INVALID",                                 -1);

exports.registeredAsFlag = {};
define(exports.registeredAsFlag, "DRIVER",                     1);
define(exports.registeredAsFlag, "CUSTOMER",                   0);

define(exports, "DELETED_STATUS",                           1);
define(exports, "REGISTRATION_DRIVER_STATUS",               1);
define(exports, "LIMO_CAR_FLAG",                            0);
define(exports, "VAN_CAR_FLAG",                             1);

//BLOCK/UNBLOCK
define(exports, "BLOCK_STATUS",                             1);
define(exports, "UNBLOCK_STATUS",                           0);

//INITIAL LAT/LONG FOR DRIVER
define(exports, "INITIAL_LATITUDE",                            0);
define(exports, "INITIAL_LONGITUDE",                           0);


exports.engagementStatus = {};
define(exports.engagementStatus, "REQUESTED",                   0 );    // request has been sent
define(exports.engagementStatus, "ACCEPTED",                    1 );    // request has been accepted by the driver
define(exports.engagementStatus, "STARTED",                     2 );    // ride has started
define(exports.engagementStatus, "ENDED",                       3 );    // ride has ended
define(exports.engagementStatus, "REJECTED_BY_DRIVER",          4 );    // request rejected by driver
define(exports.engagementStatus, "CANCELLED_BY_CUSTOMER",       5 );    // request cancelled by customer
define(exports.engagementStatus, "TIMEOUT",                     6 );    // request timed out
define(exports.engagementStatus, "ACCEPTED_BY_OTHER_DRIVER",    7 );    // request was accepted by another driver
define(exports.engagementStatus, "ACCEPTED_THEN_REJECTED",      8 );    // request was accepted and then rejected
define(exports.engagementStatus, "CLOSED",                      9 );    // request was closed when the driver accepted other request
define(exports.engagementStatus, "CANCELLED_ACCEPTED_REQUEST",  10);    // request was cancelled after it was accepted by a driver


exports.userFreeStatus = {};
define(exports.userFreeStatus, "FREE",    0);
define(exports.userFreeStatus, "BUSY",    1);

exports.userVerificationStatus = {};
define(exports.userVerificationStatus, "VERIFY",                     1);
define(exports.userVerificationStatus, "NOTVERIFY",                  0);

exports.sessionStatus = {};
define(exports.sessionStatus, "INACTIVE",   0);
define(exports.sessionStatus, "ACTIVE",     1);
define(exports.sessionStatus, "TIMED_OUT",  2);

exports.userCurrentStatus = {};
define(exports.userCurrentStatus, "OFFLINE",            0);
define(exports.userCurrentStatus, "DRIVER_ONLINE",      1);
define(exports.userCurrentStatus, "CUSTOMER_ONLINE",    2);

exports.userRegistrationStatus = {};
define(exports.userRegistrationStatus, "CUSTOMER",          0);
define(exports.userRegistrationStatus, "DRIVER",            1);
define(exports.userRegistrationStatus, "DEDICATED_DRIVER",  2);

exports.rideAcceptanceFlag = {};
define(exports.rideAcceptanceFlag, "NOT_YET_ACCEPTED",          0);
define(exports.rideAcceptanceFlag, "ACCEPTED",                  1);
define(exports.rideAcceptanceFlag, "ACCEPTED_THEN_REJECTED",    2);

exports.notificationFlags = {};
define(exports.notificationFlags, "REQUEST",                        0 );  //driver
define(exports.notificationFlags, "REQUEST_TIMEOUT",                1 );  //driver
define(exports.notificationFlags, "REQUEST_CANCELLED",              2 );  //driver
define(exports.notificationFlags, "RIDE_STARTED",                   3 );  //customer
define(exports.notificationFlags, "RIDE_ENDED",                     4 );  //customer
define(exports.notificationFlags, "RIDE_ACCEPTED",                  5 );  //customer
define(exports.notificationFlags, "RIDE_ACCEPTED_BY_OTHER_DRIVER",  6 );  //driver
define(exports.notificationFlags, "RIDE_REJECTED_BY_DRIVER",        7 );  //customer
define(exports.notificationFlags, "NO_DRIVERS_AVAILABLE",           8 );  //customer
define(exports.notificationFlags, "WAITING_STARTED",                9 );  //customer
define(exports.notificationFlags, "WAITING_ENDED",                  10);  //customer

define(exports.notificationFlags, "SPLIT_FARE_REQUEST",             11);
define(exports.notificationFlags, "CHANGE_STATE",                   20);
define(exports.notificationFlags, "DISPLAY_MESSAGE",                21);
define(exports.notificationFlags, "TOGGLE_LOCATION_UPDATES",        22);
define(exports.notificationFlags, "MANUAL_ENGAGEMENT",              23);
define(exports.notificationFlags, "HEARTBEAT",                      40);
define(exports.notificationFlags, "STATION_CHANGED",                50);
define(exports.notificationFlags, "PAYMENT_INFO_TO_DRIVER",         51);

exports.deviceType = {};
define(exports.deviceType, "ANDROID",   0);
define(exports.deviceType, "iOS",       1);