var commonFunc = require('./commonfunction');
var logging = require('./logging');
var responses = require('./responses');

/*
 * -----------------------------------------------
 * GET ALL DRIVER
 * -----------------------------------------------
 */

exports.get_all_customer = function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    var access_token = req.body.access_token;
    var manvalues = [access_token];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateAdminAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                var sql = "SELECT `user_id` AS `customer_id`,`user_name`,`user_email`,`phone_no`,";
                sql += "`total_rides_as_user`,`total_rating_got_user`,`total_rating_user`,"
                sql += "`req_stat` AS `registration_status`,`is_blocked`,`is_deleted` FROM `tb_users` WHERE ";
                sql += "`reg_as`=? AND `make_me_driver_flag`=? AND `is_deleted`<>?";
                var bindparams = [constants.registeredAsFlag.CUSTOMER, constants.makeMeDriverFlag.APPROVED_DRIVER_STATUS, constants.DELETED_STATUS];
                connection.query(sql, bindparams, function (err, getAllCustomer) {
                    if (err) {
                        logging.logDatabaseQueryError("Error in fetching all driver : ", err, getAllCustomer);
                        responses.sendError(res);
                        return;
                    } else {
                        if (getAllCustomer.length > 0) {
                            var response = {
                                "message": constants.responseMessages.ACTION_COMPLETE,
                                "status": constants.responseFlags.ACTION_COMPLETE,
                                "data": {"customer_list": getAllCustomer}
                            };
                        } else {
                            var response = {
                                "message": constants.responseMessages.ACTION_COMPLETE,
                                "status": constants.responseFlags.ACTION_COMPLETE,
                                "data": {"customer_list": []}
                            };
                        }
                        res.send(JSON.stringify(response));
                        return;
                    }
                });
            }
        });
    }
};