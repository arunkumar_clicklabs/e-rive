var commonFunc = require('./commonfunction');
var logging = require('./logging');
var responses = require('./responses');
var async = require('async');

/*
 * -----------------------------------------------
 * DRIVERS AND THEIR PAYMENT DETAILS
 * -----------------------------------------------
 */

exports.driver_payment_details = function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    var access_token = req.body.access_token;
    var manvalues = [access_token];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateAdminAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                var sql = "SELECT `user_id` AS `driver_id`,`user_name`,`phone_no`,`user_email`,`driver_paid_payments`,`total_earnings`,`driver_cash_earnings`";
                sql +=",(`total_earnings`-(`driver_paid_payments`+`driver_cash_earnings`)) as `remaining_amount_paid_to_driver` FROM `tb_users` ";
                sql += "WHERE `reg_as`=? AND is_deleted = 0";
                connection.query(sql, [constants.registeredAsFlag.DRIVER], function (err, driverheatmapresult) {
                    if (err) {
                        logging.logDatabaseQueryError("Error in fetching unapproved driver : ", err, driverheatmapresult);
                        responses.sendError(res);
                        return;
                    }
                    if (result.length > 0) {
                        var response = {
                            "message": constants.responseMessages.ACTION_COMPLETE,
                            "status": constants.responseFlags.ACTION_COMPLETE,
                            "data": {"driver_data": driverheatmapresult}
                        };
                    } else {
                        var response = {
                            "message": constants.responseMessages.SHOW_ERROR_MESSAGE,
                            "status": constants.responseFlags.SHOW_ERROR_MESSAGE,
                            "data": {"driver_data": []}
                        };
                    }
                    res.send(JSON.stringify(response));
                    return;
                });
            }
        });
    }
};

/*
 * -----------------------------------------------
 * PAY TO DRIVER
 * -----------------------------------------------
 */

exports.payDriver = function(req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    var access_token = req.body.access_token;
    var driverId = req.body.driver_id;
    var amount = req.body.amount;
    var flag = req.body.flag;// 0 manually payment 1 by paypal or other bank related
    var manvalues = [access_token,driverId,amount];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateAdminAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                payDriverFunction(amount,driverId,flag, req, res);
            }
        });
    }
};

function payDriverFunction( amount, driverId,flag, req, res) {
            var amountPaid = (amount);
            var response = {
                "message": constants.responseMessages.ACTION_COMPLETE,
                "status": constants.responseFlags.ACTION_COMPLETE,
                "data": {}
            };
            var sql1 = "UPDATE tb_users SET `driver_paid_payments` = `driver_paid_payments` + ? WHERE user_id = ?";
            connection.query(sql1, [amountPaid,driverId], function(err, update1) {
                res.send(JSON.stringify(response));
            });
            return;
}